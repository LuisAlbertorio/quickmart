package com.pixnabi.quickmart.myapplication;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;


public class AccountActivity extends BaseActivity {

    EditText nombre, apellido, telefono, zipcode, email;
    String nombreString, apellidoString, telefonoString, zipcodeString, emailString;
    ParseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);
        //setContentView(R.layout.account_activity);

        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");
        ParseFacebookUtils.initialize("960454483983536");

        user = ParseUser.getCurrentUser();

        // Fetch Facebook user info if the session is active
        /*Session session = ParseFacebookUtils.getSession();
        if (session != null && session.isOpened()) {
            makeMeRequest();
            // newMyFriendsRequest();
        }*/

        nombre = (EditText) findViewById(R.id.nombre);
        nombre.setText(user.get("nameUser").toString());

        apellido = (EditText) findViewById(R.id.apellido);
        apellido.setText(user.get("apellidosUser").toString());

        telefono = (EditText) findViewById(R.id.telefono);

        if (user.get("telefonoUser") == null)     {
            telefono.setText("");
        }else {
            telefono.setText(user.get("telefonoUser").toString());
        }

        zipcode = (EditText) findViewById(R.id.zipcode);
        zipcode.setText(user.get("zipcodeUser").toString());

        email = (EditText) findViewById(R.id.email);
        if (ParseFacebookUtils.isLinked(user)) {
            if (user.get("emailUser") == null) {
                email.setText("");
            } else {
                email.setText(user.get("emailUser").toString());
            }
        }else{
            if (user.getUsername() == null) {
                email.setText("");
            } else {
                email.setText(user.getUsername());
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Account");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


    }

    public void guardarOnClick (View view){

        nombreString = nombre.getText().toString();
        apellidoString = apellido.getText().toString();
        telefonoString = telefono.getText().toString();
        zipcodeString = zipcode.getText().toString();
        emailString = email.getText().toString();

        user.put("nameUser", nombreString);
        user.put("apellidosUser", apellidoString);
        user.put("telefonoUser", telefonoString);
        user.put("zipcodeUser", zipcodeString);
        user.put("emailUser", emailString);
        //user.setUsername(emailString);
        user.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null){
                    Toast.makeText(getBaseContext(), "Guardado", Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    Toast.makeText(getBaseContext(), "No se pudo guardar.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.account_activity;
    }

    private void makeMeRequest() {
        Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        if (user != null) {
                            // Create a JSON object to hold the profile info
                            JSONObject userProfile = new JSONObject();
                            try {
                                // Populate the JSON object
                                userProfile.put("facebookId", user.getId());
                                userProfile.put("name", user.getName());

                                Toast.makeText(getBaseContext(), "" + user.getFirstName(), Toast.LENGTH_SHORT).show();


                            } catch (JSONException e) {
                                //Log.d(IntegratingFacebookTutorialApplication.TAG,
                                //"Error parsing returned user data.");
                            }

                        } else if (response.getError() != null) {
                            // handle error
                        }
                    }
                });
        request.executeAsync();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.simple_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        finish();

        return super.onOptionsItemSelected(item);
    }
}


