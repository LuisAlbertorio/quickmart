package com.pixnabi.quickmart.myapplication;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class AddressActivity extends BaseActivity {

    Toolbar toolbar;
    EditText label, street, urb, city, zipcode, deliveryInstruction;
    String labelString, streetString, urbString, cityString, zipcodeString, deliveryString;
    ParseUser currentUser;
    String addressId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);

        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");

        currentUser = ParseUser.getCurrentUser();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setNavigationIcon(R.drawable.ic_drawer);
        toolbar.setTitle("Dirección");
        toolbar.setTitleTextColor(Color.parseColor("#616161"));
        toolbar.canShowOverflowMenu();

        label = (EditText) findViewById(R.id.label);

        street = (EditText) findViewById(R.id.streetAddress);

        urb = (EditText) findViewById(R.id.urb);

        city = (EditText) findViewById(R.id.ciudad);

        zipcode = (EditText) findViewById(R.id.zipcode);

        deliveryInstruction = (EditText) findViewById(R.id.instrucciones);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            addressId = extras.getString("addressId");

            label.setText(extras.getString("label"));
            street.setText(extras.getString("street"));
            urb.setText(extras.getString("urb"));
            city.setText(extras.getString("city"));
            zipcode.setText(extras.getString("zipcode"));
            deliveryInstruction.setText(extras.getString("deliveryInstructions"));

        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.address_activity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.save_address, menu);

        RelativeLayout badgeLayout = (RelativeLayout) menu.findItem(R.id.saveAddress).getActionView();

        Button saveAddress = (Button) badgeLayout.findViewById(R.id.saveAddress);
        saveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getBaseContext(), "Guardar!!!", Toast.LENGTH_SHORT).show();

                labelString = label.getText().toString();
                streetString = street.getText().toString();
                urbString = urb.getText().toString();
                cityString = city.getText().toString();
                zipcodeString = zipcode.getText().toString();
                deliveryString = deliveryInstruction.getText().toString();

                ParseObject address = new ParseObject("Address");

                if (addressId != null){
                    //address.put("objectId", addressId);
                    address.setObjectId(addressId);
                }

                address.put("label", labelString);
                address.put("street", streetString);
                address.put("urb", urbString);
                address.put("city", cityString);
                address.put("zipcode", zipcodeString);
                address.put("deliveryInstructions", deliveryString);
                address.put("userId", currentUser.getObjectId());
                //address.saveInBackground();
                address.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null){
                            Intent intent = new Intent();
                            setResult(RESULT_OK, intent);
                            finish();
                        }else{
                            Toast.makeText(getBaseContext(), "No se pudo guardar.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.saveAddress) {


            return true;

        }else {

            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
