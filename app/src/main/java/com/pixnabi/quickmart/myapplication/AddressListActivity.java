package com.pixnabi.quickmart.myapplication;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

public class AddressListActivity extends BaseActivity {

    Toolbar toolbar;
    RecyclerView mRecyclerView;
    AddreesListAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    ParseUser currentUser;
    List<ParseObject> addresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);

        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");
        currentUser = ParseUser.getCurrentUser();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setNavigationIcon(R.drawable.ic_drawer);
        toolbar.setTitle("Direcciones");
        toolbar.setTitleTextColor(Color.parseColor("#616161"));
        toolbar.canShowOverflowMenu();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new AddreesListAdapter(this, this);
        mRecyclerView.setAdapter(mAdapter);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Address");
        query.whereEqualTo("userId", currentUser.getObjectId());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {

                if (e == null) {
                    Log.d("score", "The getFirst request failed.");
                    addresses = parseObjects;
                    mAdapter.reloadWithData(addresses);
                } else {
                    Log.d("score", "Retrieved the object.");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                //reload data
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Address");
                query.whereEqualTo("userId", currentUser.getObjectId());
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> parseObjects, ParseException e) {

                        if (e == null) {
                            Log.d("score", "The getFirst request failed.");
                            addresses = parseObjects;
                            mAdapter.reloadWithData(addresses);
                        } else {
                            Log.d("score", "Retrieved the object.");
                        }
                    }
                });
            }
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.address_list_activity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_direction, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.addDireccion) {

            Intent intent = new Intent(AddressListActivity.this, AddressActivity.class);
            startActivityForResult(intent, 1);

            return true;

        }else {

            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
