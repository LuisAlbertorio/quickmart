package com.pixnabi.quickmart.myapplication;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class CartActivity extends BaseActivity {

    RecyclerView mRecyclerView;
    CartRecycleViewAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    final int PRODUCT_BY_QUANTITY = 1;
    final int PRODUCT_BY_WEIGHT = 2;

    int productType;
    int quantity;
    float weight;
    int currentQuantity;
    float currentWeight;

    TextView total, itemsName;
    TextView counter, numeroOrden, fechaOrden;
    ImageButton plus, minus, close, closeCheckout;
    Button inicialTimePicker, finalTimePicker, addToCart;
    Calendar timeNow;
    AlertDialog checkoutPopup;
    AlertDialog alertDialog;
    Button creditCardBtn, paypalBtn;

    ArrayList<ParseObject> shoppingCart;
    ParseUser currentUser;
    int selectedItemPosition;
    int amountincart;

    SimpleDateFormat sdf;
    SimpleDateFormat time;
    String ti;
    String tf;

    Date initTime;
    Date currentTime;
    Date finalTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);
        //setContentView(R.layout.activity_home)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        //parse initializing
        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");
        currentUser = ParseUser.getCurrentUser();

        amountincart = getIntent().getExtras().getInt("amountincart");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Cart");
        toolbar.setTitleTextColor(Color.parseColor("#616161"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        total = (TextView) findViewById(R.id.total);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new CartRecycleViewAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        timeNow = Calendar.getInstance();

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("userId", currentUser.getObjectId());

        ParseCloud.callFunctionInBackground("shoppingCart", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {

            public void done(ArrayList<ParseObject> result, ParseException e) {
                if (e == null) {

                    if(result.size() > 0) {
                        ParseObject order = result.get(0);
                        total.setText("$" + String.format("%.2f", order.getDouble("total")) );

                    }
                    else {
                        total.setText("$0.00");
                    }

                    shoppingCart = result;
                    if(shoppingCart.size() != 0) amountincart = shoppingCart.size();
                    mAdapter.reloadWithData(shoppingCart);
                } else {
                    // an error occur
                    Toast.makeText(getApplicationContext(), "Error Message: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        //for paypal payment
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    public void onClickRowCart (View view){

        selectedItemPosition = ((int) view.getTag()) + 1;


        ImageView imageView;
        TextView itemName, itemdesc;


        ParseObject item = shoppingCart.get(selectedItemPosition);

        if(item.getString("productType").contentEquals("by unit")) {
            productType = PRODUCT_BY_QUANTITY;
            currentQuantity = (int) item.getDouble("quantity");
        }
        else {
            productType = PRODUCT_BY_WEIGHT;
            currentWeight = (float) item.getDouble("quantity");
        }

        quantity = currentQuantity;
        weight = currentWeight;

        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.show();
        alertDialog.setContentView(R.layout.popup_cart_items);
        imageView = (ImageView)alertDialog.findViewById(R.id.imageItems);
        itemName = (TextView)alertDialog.findViewById(R.id.itemsName);
        itemdesc = (TextView)alertDialog.findViewById(R.id.flappy);

        ParseFile photoFile = item.getParseFile("image");
        Picasso.with(this).load(photoFile.getUrl()).into(imageView);

        itemName.setText(item.getString("name"));
        String desc = "$"+String.valueOf(item.getDouble("price")) + " " + item.getString("unit");
        if(item.getString("unit") == null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(Objects.equals(item.getString("weightUnit"), "lb"))
                 desc = "$"+String.valueOf(item.getDouble("price")) + " per lb";
            }

        }
        itemdesc.setText(desc);


        counter = (TextView) alertDialog.findViewById(R.id.counter);
        if(productType == PRODUCT_BY_QUANTITY) {
            counter.setText("" + quantity);
        }
        else {
            counter.setText("" + weight);
        }

        plus = (ImageButton) alertDialog.findViewById(R.id.plus);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plus();
            }
        });

        minus = (ImageButton) alertDialog.findViewById(R.id.minus);
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minus();
            }
        });

        addToCart = (Button) alertDialog.findViewById(R.id.add_to_cart);
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateItemInCart(v);
            }
        });

        close = (ImageButton) alertDialog.findViewById(R.id.closePopup);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });

        Toast.makeText(getBaseContext(), "Item Position: " + selectedItemPosition, Toast.LENGTH_SHORT).show();
    }

    public void updateItemInCart( View view) {

        ParseObject item = shoppingCart.get(selectedItemPosition);

        HashMap hashMap = new HashMap<String, Object>();
        hashMap.put("userId", currentUser.getObjectId());
        hashMap.put("productId", item.getString("productId"));
        //hashMap.put("quantity",20);

        if(productType == PRODUCT_BY_QUANTITY) {
            hashMap.put("quantity", quantity);
        }
        else {
            hashMap.put("quantity", weight);
        }

        ParseCloud.callFunctionInBackground("addProductToCart", hashMap, new FunctionCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {

                if (e == null) {

                    Toast.makeText(getApplicationContext(), "Product: " + parseObject.toString(), Toast.LENGTH_SHORT).show();
                    alertDialog.cancel();
                    alertDialog = null;

                    //update row with new quantity
                    mAdapter.shoppingCart.set(selectedItemPosition, parseObject);
                    mAdapter.notifyItemChanged(selectedItemPosition - 1);

                    HashMap<String, Object> hashMap2 = new HashMap<String, Object>();
                    hashMap2.put("userId", currentUser.getObjectId());

                    ParseCloud.callFunctionInBackground("shoppingCart", hashMap2, new FunctionCallback<ArrayList<ParseObject>>() {

                        public void done(ArrayList<ParseObject> result, ParseException e) {
                            if (e == null) {

                                if(result.size() > 0) {
                                    ParseObject order = result.get(0);
                                    total.setText("$" + String.format("%.2f", order.getDouble("total")) );

                                }
                                else {
                                    total.setText("$0.00");
                                }

                                shoppingCart = result;
                                if(shoppingCart.size() != 0) amountincart = shoppingCart.size();
                                mAdapter.reloadWithData(shoppingCart);
                            } else {
                                // an error occur
                                Toast.makeText(getApplicationContext(), "Error Message: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    //for update all rows
                    //shoppingCart.set(selectedItemPosition, parseObject);
                    //mAdapter.notifyDataSetChanged();
                } else {
                    // an error occur
                    Toast.makeText(getApplicationContext(), "Error Message: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    public void plus (){

        if(productType == PRODUCT_BY_QUANTITY) {
            quantity++;
            counter.setText(String.valueOf(quantity));
        }
        else {
            weight += 0.25;
            counter.setText(Float.toString(weight));
        }
    }

    public void minus (){

        if(productType == PRODUCT_BY_QUANTITY) {
            if (quantity <= 0) {

                counter.setText("0");

            } else
                quantity--;
            counter.setText(String.valueOf(quantity));
        }
        else {
            if(weight <= 0) {
                counter.setText("0");
            }
            else {
                weight -= 0.25;
                if(weight <= 0)
                    counter.setText("0");
                else
                    counter.setText(Float.toString(weight));
            }
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.cart_activity;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.checkout_menu, menu);

        RelativeLayout badgeLayout = (RelativeLayout) menu.findItem(R.id.checkoutCart).getActionView();

        Log.d("min compra", total.getText().toString().replace("$", ""));

        Button pagar = (Button) badgeLayout.findViewById(R.id.pagar);
        pagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getBaseContext(), "Checkout!!!", Toast.LENGTH_SHORT).show();
                Log.d("min compra", total.getText().toString().replace("$", ""));
                if(Double.parseDouble(total.getText().toString().replace("$", "")) < 35.00) {
                    Toast.makeText(CartActivity.this, "El minimo de compra es de $35.00", Toast.LENGTH_LONG).show();
                }else{

                    final AlertDialog alertHorarioEntrega = new AlertDialog.Builder(CartActivity.this).create();

                    alertHorarioEntrega.show();
                    alertHorarioEntrega.setContentView(R.layout.popup_horario_entrega);

                    ImageButton close = (ImageButton) alertHorarioEntrega.findViewById(R.id.closePopupHorario);
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertHorarioEntrega.cancel();
                        }
                    });

                    Button aceptar = (Button) alertHorarioEntrega.findViewById(R.id.aceptar);
                    aceptar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            alertHorarioEntrega.cancel();

                            final AlertDialog alertDialog = new AlertDialog.Builder(CartActivity.this).create();

                            checkoutPopup = alertDialog;
                            alertDialog.show();
                            alertDialog.setContentView(R.layout.checkout_popup);

                            Calendar tiempo = Calendar.getInstance();

                            sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                            time = new SimpleDateFormat("h:mm a", Locale.US);

                            int mHour = tiempo.get(Calendar.HOUR_OF_DAY);
                            int mMinute = tiempo.get(Calendar.MINUTE);

                            tiempo.set(Calendar.HOUR_OF_DAY, mHour + 4);
                            tiempo.set(Calendar.MINUTE, mMinute);

                            tf = time.format(tiempo.getTime());

                            tiempo.set(Calendar.HOUR_OF_DAY, mHour + 2);
                            tiempo.set(Calendar.MINUTE, mMinute);

                            ti = time.format(tiempo.getTime());

                            try {
                                initTime = time.parse(time.format(tiempo.getTime()));
                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }

                            try {
                                currentTime = time.parse(time.format(Calendar.getInstance().getTime()));
                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }

                            creditCardBtn = (Button)alertDialog.findViewById(R.id.add_to_cart);
                            paypalBtn = (Button)alertDialog.findViewById(R.id.paypal);

                            //numeroOrden = (TextView) alertDialog.findViewById(R.id.numeroOrden);
                            //numeroOrden.setText(shoppingCart.get(0).getObjectId());

                            fechaOrden = (TextView) alertDialog.findViewById(R.id.fechaOrden);
                            fechaOrden.setText(sdf.format(Calendar.getInstance().getTime()));

                            final ParseObject totalPopup = shoppingCart.get(0);

                            itemsName = (TextView) alertDialog.findViewById(R.id.itemsName);
                            itemsName.setText("Total: $" + String.format("%.2f", totalPopup.getDouble("total")));
//                            itemsName.setText(total.getText());

                            inicialTimePicker = (Button) alertDialog.findViewById(R.id.firstTimePicker);
                            inicialTimePicker.setText(ti);
                            inicialTimePicker.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    inicialTime(v);
                                }
                            });

                            finalTimePicker = (Button) alertDialog.findViewById(R.id.secondTimePicker);
                            finalTimePicker.setText(tf);
                            finalTimePicker.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finalTime(v);
                                }
                            });

                            paypalBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
//                                    Log.d("time difference", time.format(Calendar.getInstance().getTime()) + " " + ti);

                                    long difference = initTime.getTime() - currentTime.getTime();
                                    Log.d("current Time", String.valueOf(currentTime.getTime()));
                                    Log.d("long difference", String.valueOf(difference));

                                    int days = (int) (difference / (1000*60*60*24));
                                    int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
                                    int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
                                    Log.i("log_tag","Hours: "+hours+", Mins: "+min);

                                    if(hours <= 0 || min <= 0){
                                        Toast.makeText(CartActivity.this, "Ya esa hora paso, escoge otra hora", Toast.LENGTH_LONG).show();
                                    } else if(hours < 2 && hours >= 0){
                                        Toast.makeText(CartActivity.this, "El delivery tiene que ser 2 horas despues de la orden", Toast.LENGTH_LONG).show();
                                    }else{
                                        Toast.makeText(CartActivity.this, "Eureka", Toast.LENGTH_LONG).show();
                                        getPayment(totalPopup.getDouble("total"));
                                    }

                                }
                            });

                            closeCheckout = (ImageButton) alertDialog.findViewById(R.id.closePopupCheckout);
                            closeCheckout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.cancel();
                                }
                            });
                        }
                    });
                }
            }
        });
        return true;
    }

    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 123;


    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)
            .acceptCreditCards(false)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    public void getPayment(double total){
        //Getting the amount from editText
//        paymentAmount = editTextAmount.getText().toString();

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(total), "USD", "quickmart",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //finish();
        //Toast.makeText(getBaseContext(), "Eureka!!!", Toast.LENGTH_SHORT).show();

        if (id == R.id.checkoutCart) {

            return true;
        }else {
            Intent intent = new Intent();
            intent.putExtra("amountincart", shoppingCart.size()-1);
            setResult(RESULT_OK, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void inicialTime (View v){

        final Button txtTime = (Button) checkoutPopup.findViewById(R.id.firstTimePicker);
        //final Calendar c = Calendar.getInstance();
        int  mHour = timeNow.get(Calendar.HOUR_OF_DAY);
        int mMinute = timeNow.get(Calendar.MINUTE);


        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        timeNow.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        timeNow.set(Calendar.MINUTE, minute);
                        SimpleDateFormat sdf = new SimpleDateFormat ("h:mm a", Locale.US);
                        String fecha = sdf.format(timeNow.getTime());
                        //Toast.makeText(getBaseContext(),"" + fecha, Toast.LENGTH_LONG).show();
                        try {
                            initTime = sdf.parse(sdf.format(timeNow.getTime()));
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }
                        txtTime.setText(fecha);
                        ti = fecha;
                    }
                },mHour, mMinute, false);
        tpd.show();
    }

    public void finalTime (View v){

        final Button txtTime = (Button) checkoutPopup.findViewById(R.id.secondTimePicker);
        //final Calendar c = Calendar.getInstance();
        int  mHour = timeNow.get(Calendar.HOUR_OF_DAY);
        int mMinute = timeNow.get(Calendar.MINUTE);

        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        timeNow.set(Calendar.HOUR_OF_DAY , hourOfDay);
                        timeNow.set(Calendar.MINUTE, minute);
                        SimpleDateFormat sdf = new SimpleDateFormat ("h:mm a", Locale.US);
                        String fecha = sdf.format(timeNow.getTime());
                        //Toast.makeText(getBaseContext(),"" + fecha, Toast.LENGTH_LONG).show();
                        txtTime.setText(fecha);
                        tf = fecha;
                    }
                },mHour + 2, mMinute, false);
        tpd.show();
    }

    @Override
    public void onBackPressed(){

        Intent intent = new Intent();
        intent.putExtra("amountincart", shoppingCart.size()-1);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        //Starting a new activity for the payment details and also putting the payment details with intent
                        startActivity(new Intent(this, ConfirmationActivity.class)
                                .putExtra("PaymentDetails", paymentDetails)
                                .putExtra("PaymentAmount", total.getText().toString()));
                        finish();

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

}
