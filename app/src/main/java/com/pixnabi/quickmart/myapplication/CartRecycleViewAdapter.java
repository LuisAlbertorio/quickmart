package com.pixnabi.quickmart.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.ArrayList;


public class CartRecycleViewAdapter extends RecyclerView.Adapter<CartRecycleViewAdapter.ViewHolder> {
    //private String[] mDataset;
    //private int[] mCars;
    public ArrayList<ParseObject> shoppingCart;
    public boolean isShoppingCart;
    Context mContext;
    TextView total;
    public ParseObject itemForDel;
    public CartActivity cartActivity;

    // Provide a suitable constructor (depends on the kind of dataset)
    public CartRecycleViewAdapter(Context context) {
        //mDataset=myDataset;
        //mCars = cars;

        //is true order is pending so we can delete article is user shopping cart otherwise is a buyed order and items cannot be deleted
        isShoppingCart = true;
        mContext = context;
    }

    public void reloadWithData(ArrayList<ParseObject> shoppingCart2) {
        Log.d("reloadSize", String.valueOf(shoppingCart2.size()));
        shoppingCart = shoppingCart2;

        notifyDataSetChanged();
    }


// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder

    public  class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        RelativeLayout relativeLayout;
        TextView itemQuantity;
        TextView itemName;
        TextView itemDescription;
        TextView itemPrice;
        ImageView itemImage;
        ImageButton itemDelete;


        public ViewHolder(View v) {
            super(v);

            relativeLayout = (RelativeLayout) v.findViewById(R.id.relativeRowCart);
            itemQuantity = (TextView) v.findViewById(R.id.item_quantity);
            itemName = (TextView) v.findViewById(R.id.item_name);
            itemDescription = (TextView) v.findViewById(R.id.item_description);
            //itemPrice = (TextView) v.findViewById(R.id.item_price);
            itemImage = (ImageView) v.findViewById(R.id.item_imageview);
            itemDelete = (ImageButton) v.findViewById(R.id.item_delete);
            total = (TextView)v.findViewById(R.id.total);

            itemDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteItem(getPosition());
                }
            });

        }

    }

    public void deleteItem(int pos){
        cartActivity = (CartActivity) mContext;
        itemForDel = shoppingCart.get(pos + 1);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Order");
        query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
        query.whereEqualTo("objectId", itemForDel.getString("orderId"));

//        Toast.makeText(mContext, "name: " + itemForDel.getString("name"), Toast.LENGTH_LONG).show();

        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                double ftotal = parseObject.getDouble("total") - itemForDel.getDouble("total");
                if(ftotal <= 0){
                    parseObject.put("total", 0);
                    cartActivity.total.setText("$0.00");

                }else{
                    parseObject.put("total", ftotal);
                    cartActivity.total.setText("$" + String.format("%.2f", ftotal) );
                }
                parseObject.saveInBackground();


            }
        });

        query = ParseQuery.getQuery("Cart");
        query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
        query.whereEqualTo("objectId", itemForDel.getObjectId());

        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                try {
                    parseObject.delete();
                } catch (com.parse.ParseException e1) {
                    e1.printStackTrace();
                }
                parseObject.saveInBackground();
            }
        });

        shoppingCart.remove(pos + 1);
        notifyItemRemoved(pos);
        notifyItemRangeChanged(pos, shoppingCart.size());
    }


    // Create new views (invoked by the layout manager)
    @Override
    public CartRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_items_row, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        ParseObject item = shoppingCart.get(position + 1);
        holder.relativeLayout.setTag(position);
        if(item.getString("productType").contentEquals("by unit")) {
            holder.itemQuantity.setText("" + item.getInt("quantity"));
        }
        else {
            holder.itemQuantity.setText("" + item.getDouble("quantity"));
        }

        if(isShoppingCart == false) {
            //hide delete button
            holder.itemDelete.setVisibility(View.INVISIBLE);
        }

        holder.itemName.setText(item.getString("name"));
        holder.itemDescription.setText(item.getString("description"));

        ParseFile photoFile = item.getParseFile("image");
        Picasso.with(mContext).load(photoFile.getUrl()).into(holder.itemImage);
    }

    @Override
    public int getItemCount() {
        if(shoppingCart == null || shoppingCart.size() == 0) {
            return 0;
        }

        return shoppingCart.size() - 1;
    }
}
