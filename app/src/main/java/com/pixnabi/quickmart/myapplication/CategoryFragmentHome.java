package com.pixnabi.quickmart.myapplication;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetDataCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import android.os.Handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.LogRecord;


public class CategoryFragmentHome extends android.support.v4.app.Fragment {

    HomeActivity activity;
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    String [] categories;
    ArrayList<String> cat;
    String [] productNames;
    String [] productDescription;
    String [] productId;
    String [] productImage;
    ParseFile[] parseFile;
    Bitmap[] bitmap;
    ProgressDialog progressDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);



        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycleView);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //get current categories from parse
//        getCategories();

        cat = getArguments().getStringArrayList("categories");
        productNames = getArguments().getStringArray("productNames");
        productDescription = getArguments().getStringArray("productDescription");
        productId = getArguments().getStringArray("productId");
        productImage = getArguments().getStringArray("productImage");

//        for (String productName : productNames) {
//            Log.d("PRODUCTNAMES", productName);
//        }



//        Toast.makeText(getActivity(), "Cat Size: " + String.valueOf(cat.size()), Toast.LENGTH_LONG).show();
//        Toast.makeText(getActivity(), "pnames: " + String.valueOf(productNames.length), Toast.LENGTH_LONG).show();

        mAdapter = new RecycleViewAdapter(getActivity(), cat, productNames, productDescription, productImage, productId);
        mRecyclerView.setAdapter(mAdapter);

        return rootView;
    }
}
