package com.pixnabi.quickmart.myapplication;


import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class CreditCardActivity extends BaseActivity {

    EditText cardNumber, expirationDate, cvc, street, urb, city, zipcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setNavigationIcon(R.drawable.ic_drawer);
        toolbar.setTitle("Tarjetas de Credito");
        toolbar.setTitleTextColor(Color.parseColor("#616161"));
        toolbar.canShowOverflowMenu();

        cardNumber = (EditText) findViewById(R.id.cardNumber);
        cardNumber.addTextChangedListener(new TextWatcher() {

            private boolean spaceDeleted;

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // check if a space was deleted
                CharSequence charDeleted = s.subSequence(start, start + count);
                spaceDeleted = " ".equals(charDeleted.toString());
            }

            public void afterTextChanged(Editable editable) {
                // disable text watcher
                cardNumber.removeTextChangedListener(this);

                // record cursor position as setting the text in the textview
                // places the cursor at the end
                int cursorPosition = cardNumber.getSelectionStart();
                String withSpaces = formatText(editable);
                cardNumber.setText(withSpaces);
                // set the cursor at the last position + the spaces added since the
                // space are always added before the cursor
                cardNumber.setSelection(cursorPosition + (withSpaces.length() - editable.length()));

                // if a space was deleted also deleted just move the cursor
                // before the space
                if (spaceDeleted) {
                    cardNumber.setSelection(cardNumber.getSelectionStart() - 1);
                    spaceDeleted = false;
                }

                // enable text watcher
                cardNumber.addTextChangedListener(this);
            }

            private String formatText(CharSequence text)
            {
                StringBuilder formatted = new StringBuilder();
                int count = 0;
                for (int i = 0; i < text.length(); ++i)
                {
                    if (Character.isDigit(text.charAt(i)))
                    {
                        if (count % 4 == 0 && count > 0)
                            formatted.append(" ");
                        formatted.append(text.charAt(i));
                        ++count;
                    }
                }
                return formatted.toString();
            }
        });

        expirationDate = (EditText) findViewById(R.id.expirationDate);
        expirationDate.addTextChangedListener(new TextWatcher() {

            private boolean spaceDeleted;

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // check if a space was deleted
                CharSequence charDeleted = s.subSequence(start, start + count);
                spaceDeleted = "/".equals(charDeleted.toString());
            }

            public void afterTextChanged(Editable editable) {
                // disable text watcher
                expirationDate.removeTextChangedListener(this);

                // record cursor position as setting the text in the textview
                // places the cursor at the end
                int cursorPosition = expirationDate.getSelectionStart();
                String withSpaces = formatText(editable);
                expirationDate.setText(withSpaces);
                // set the cursor at the last position + the spaces added since the
                // space are always added before the cursor
                expirationDate.setSelection(cursorPosition + (withSpaces.length() - editable.length()));

                // if a space was deleted also deleted just move the cursor
                // before the space
                if (spaceDeleted) {
                    expirationDate.setSelection(expirationDate.getSelectionStart() - 1);
                    spaceDeleted = false;
                }

                // enable text watcher
                expirationDate.addTextChangedListener(this);
            }

            private String formatText(CharSequence text)
            {
                StringBuilder formatted = new StringBuilder();
                int count = 0;
                for (int i = 0; i < text.length(); ++i)
                {
                    if (Character.isDigit(text.charAt(i)))
                    {
                        if (count % 2 == 0 && count > 0)
                            formatted.append("/");
                        formatted.append(text.charAt(i));
                        ++count;
                    }
                }
                return formatted.toString();
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.credit_card_activity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.save_address, menu);

        RelativeLayout badgeLayout = (RelativeLayout) menu.findItem(R.id.saveAddress).getActionView();

        Button saveAddress = (Button) badgeLayout.findViewById(R.id.saveAddress);
        saveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getBaseContext(), "Guardar!!!", Toast.LENGTH_SHORT).show();

            }
        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.saveAddress) {


            return true;

        }else {

            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
