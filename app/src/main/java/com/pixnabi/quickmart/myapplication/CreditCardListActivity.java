package com.pixnabi.quickmart.myapplication;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.ParseUser;

public class CreditCardListActivity extends BaseActivity {

    Toolbar toolbar;
    RecyclerView mRecyclerView;
    AddreesListAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    ParseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setNavigationIcon(R.drawable.ic_drawer);
        toolbar.setTitle("Tarjetas de Credito");
        toolbar.setTitleTextColor(Color.parseColor("#616161"));
        toolbar.canShowOverflowMenu();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new AddreesListAdapter(this, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.credit_card_list_activity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_direction, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.addDireccion) {

            Intent intent = new Intent(CreditCardListActivity.this, CreditCardActivity.class);
            startActivity(intent);

            return true;

        }else {

            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
