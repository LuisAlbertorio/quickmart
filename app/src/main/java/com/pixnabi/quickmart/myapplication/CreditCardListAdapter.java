package com.pixnabi.quickmart.myapplication;


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.List;

public class CreditCardListAdapter extends RecyclerView.Adapter<CreditCardListAdapter.ViewHolder> implements View.OnClickListener {


    private String[] mDataset;

    Context mContext;
    Activity activity;
    View v;
    public List<ParseObject> addresses;

    // Provide a suitable constructor (depends on the kind of dataset)
    public CreditCardListAdapter(Context context, Activity activity
    ) {
        //mDataset = myDataset;
        //mCars = cars;
        mContext = context;
        this.activity = activity;
    }

    public void reloadWithData(List<ParseObject> address) {
        this.addresses = address;

        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {

    }

// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        TextView addressLabel;

        public ViewHolder(View v) {
            super(v);

            addressLabel = (TextView) v.findViewById(R.id.addressLabel);
            ////imageView = (ImageView) v.findViewById(R.id.imageView);
            //gridview = (GridView) v.findViewById(R.id.gridViewItems);
            //viewMore = (Button) v.findViewById(R.id.buttonViewMore);
            ////cardCategory = (CardView) v.findViewById(R.id.cardCategory);

        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CreditCardListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        // create a new view
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_list_row, parent, false);
        // set the view's size, margins, paddings and layout parameters

        v.setOnClickListener(CreditCardListAdapter.this);

        ViewHolder vh = new ViewHolder(v);

        //vh.addressLabel.setOnClickListener(AddreesListAdapter.this);
        //vh.addressLabel.setTag(vh);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        v.setTag(position);

        //ParseObject address = addresses.get(position);

        //set row with order data
        //holder.addressLabel.setText(address.getString("label"));

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}