package com.pixnabi.quickmart.myapplication;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class DrawerLayoutAdapter extends BaseAdapter {
    private Context mContext;
    LayoutInflater layoutInflater;

    // Constructor
    public DrawerLayoutAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return 30;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {

            convertView = View.inflate(mContext, R.layout.grid_item, null);

            //imageView = new ImageView(mContext);
            //imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
            //imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //imageView.setPadding(8, 8, 8, 8);


        } else {
            //imageView = (ImageView) convertView;
        }

        //imageView.setImageResource(mThumbIds[position]);
        return convertView;
    }
}
