package com.pixnabi.quickmart.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class EnterZipcode extends Activity {

    ParseUser currentUser;

    EditText zipcode;
    String zipcodeString;
    String[] productNames, productDescription, productId, productImage;
    ArrayList<String> cat = new ArrayList<String>();
    int amountinCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_zipcode);

        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");
        ParseFacebookUtils.initialize("960454483983536");

        currentUser = ParseUser.getCurrentUser();

        zipcode = (EditText) findViewById(R.id.zipcode);
    }

    public void enterZipcode(View view) {

        zipcodeString = zipcode.getText().toString();

        currentUser.put("zipcodeUser", zipcodeString);
        currentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null){

                    if(ParseUser.getCurrentUser() != null) {

                        final ProgressDialog progressDialog = new ProgressDialog(EnterZipcode.this);
                        progressDialog.setMessage("logging in");
                        progressDialog.show();

                        HashMap hashMap = new HashMap<String, Object>();
                        hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());

                        ParseCloud.callFunctionInBackground("seeCategories", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
                            @Override
                            public void done(ArrayList<ParseObject> parseObjects, ParseException e) {
                                productNames = new String[parseObjects.size()];
                                productDescription = new String[parseObjects.size()];
                                productId = new String[parseObjects.size()];
                                productImage = new String[parseObjects.size()];

                                for (int i = 0; i < parseObjects.size(); i++) {
                                    productNames[i] = parseObjects.get(i).getString("name");
                                    productDescription[i] = parseObjects.get(i).getString("description");
                                    productId[i] = parseObjects.get(i).getObjectId();
                                    productImage[i] = parseObjects.get(i).getParseFile("image").getUrl();

                                    Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("name"));
                                    Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("category"));
                                    Log.d("SEE CAT DESCRIPTION", parseObjects.get(i).getString("description"));

                                    if (cat.contains(parseObjects.get(i).getString("category"))) {
                                    } else {
                                        cat.add(parseObjects.get(i).getString("category"));
                                    }
                                }
                            }
                        });

                        ParseQuery query;
                        query = ParseQuery.getQuery("Cart");
                        query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                        query.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> list, ParseException e) {
                                amountinCart = list.size();
                            }
                        });

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent i = new Intent(getBaseContext(), HomeActivity.class);
                                i.putExtra("cartAmount", amountinCart);
                                i.putStringArrayListExtra("categories", cat);
                                i.putExtra("productNames", productNames);
                                i.putExtra("productDescription", productDescription);
                                i.putExtra("productId", productId);
                                i.putExtra("productImage", productImage);
                                startActivity(i);
                                finish();
                                progressDialog.dismiss();
                            }
                        }, 3500);
                    }
//                    Intent intent = new Intent(getBaseContext(), HomeActivity.class);
//                    startActivity(intent);
                }else{
                    Toast.makeText(getBaseContext(), "No se pudo guardar.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
