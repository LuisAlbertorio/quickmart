package com.pixnabi.quickmart.myapplication;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;


public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expendablelist_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expendablelist_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        if(groupPosition == 0)
            //convertView.setBackgroundColor(Color.parseColor("#81c784"));
            lblListHeader.setTextColor(Color.parseColor("#81c784"));
        else if(groupPosition == 1)
            //convertView.setBackgroundColor(Color.parseColor("#90a4ae"));
            lblListHeader.setTextColor(Color.parseColor("#e57373"));
        else if(groupPosition == 2)
            //convertView.setBackgroundColor(Color.parseColor("#e57373"));
            lblListHeader.setTextColor(Color.parseColor("#fbc02d"));
        else if(groupPosition == 3)
            //convertView.setBackgroundColor(Color.parseColor("#64b5f6"));
            lblListHeader.setTextColor(Color.parseColor("#BA68C8"));
        else if(groupPosition == 4)
            //convertView.setBackgroundColor(Color.parseColor("#fdd835"));
            lblListHeader.setTextColor(Color.parseColor("#900D47A1"));
        else if(groupPosition == 5)
            //convertView.setBackgroundColor(Color.parseColor("#4dd0e1"));
            lblListHeader.setTextColor(Color.parseColor("#9e9e9e"));
        else if(groupPosition == 6)
            //convertView.setBackgroundColor(Color.parseColor("#a1887f"));
            lblListHeader.setTextColor(Color.parseColor("#FF8A65"));
        else if(groupPosition == 7)
            //convertView.setBackgroundColor(Color.parseColor("#f06292"));
            lblListHeader.setTextColor(Color.parseColor("#8d6e63"));
        else if(groupPosition == 8)
            //convertView.setBackgroundColor(Color.parseColor("#ffb74d"));
            lblListHeader.setTextColor(Color.parseColor("#26c6da"));
        else if(groupPosition == 9)
            //convertView.setBackgroundColor(Color.parseColor("#9e9e9e"));
            lblListHeader.setTextColor(Color.parseColor("#FF8F00"));
        else if(groupPosition == 10)
            //convertView.setBackgroundColor(Color.parseColor("#9e9e9e"));
            lblListHeader.setTextColor(Color.parseColor("#ec407a"));
        else if(groupPosition == 11)
            //convertView.setBackgroundColor(Color.parseColor("#9e9e9e"));
            lblListHeader.setTextColor(Color.parseColor("#78909C"));
        else if(groupPosition == 12)
            //convertView.setBackgroundColor(Color.parseColor("#9e9e9e"));
            lblListHeader.setTextColor(Color.parseColor("#D4E157"));

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
