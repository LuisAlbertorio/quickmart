package com.pixnabi.quickmart.myapplication;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class FavoritesItemsActivity extends BaseActivity {

    Toolbar toolbar;
    ArrayList<ParseObject> parseObject;
    FavoritesItemsAdapter favoritesItemsAdapter;
    GridView gridView;
    HomeActivity homeActivity= new HomeActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        //setContentView(R.layout.account_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setNavigationIcon(R.drawable.ic_drawer);
        toolbar.setTitle("Favoritos");
        toolbar.setTitleTextColor(Color.parseColor("#616161"));
        toolbar.canShowOverflowMenu();
//
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Getting favorites..");
//        progressDialog.show();

        final ParseQuery parseQuery = new ParseQuery("Favorites");
        parseQuery.whereEqualTo("USERID", ParseUser.getCurrentUser());
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                parseObject = new ArrayList<>(list.size());
                for(int i=0;i<list.size();i++){
                    parseObject.add(i, list.get(i));
                }
                gridView = (GridView) findViewById(R.id.gridView);


                favoritesItemsAdapter = new FavoritesItemsAdapter(FavoritesItemsActivity.this, parseObject, gridView);
                gridView.setAdapter(favoritesItemsAdapter);

//                progressDialog.dismiss();

        }
        });

        //gridView.setBackgroundColor(Color.parseColor("#81c784"));
        //gridView.setOnItemClickListener(onItemGridViewClick());
        //gridView.setOnItemClickListener(onItemGridViewClick());
    }

    public void deletingItems(int i){
        parseObject.remove(parseObject.get(i));
        favoritesItemsAdapter.notifyDataSetChanged();

        favoritesItemsAdapter = new FavoritesItemsAdapter(FavoritesItemsActivity.this, parseObject, gridView);
        gridView.invalidateViews();
        gridView.setAdapter(favoritesItemsAdapter);
    }




    @Override
    protected int getLayoutResource() {
        return R.layout.favorites_activity;
    }

    public AdapterView.OnItemClickListener onItemGridViewClick(){

        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ParseQuery query = new ParseQuery("Products");
                //query.whereEqualTo("objectId", parseObject[position].getObjectId());

                Intent intent = new Intent(FavoritesItemsActivity.this, ShopItems.class);
                try {
                    intent.putExtra("productName", query.get(parseObject.get(position).getObjectId()).getString("name"));
                    intent.putExtra("productDescription", query.get(parseObject.get(position).getObjectId()).getString("description"));
                    intent.putExtra("productImage", query.get(parseObject.get(position).getObjectId()).getParseFile("image").getUrl());
                    intent.putExtra("productId", parseObject.get(position).getObjectId());
                    intent.putExtra("productType", 1);
                    intent.putExtra("fromfavs", true);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //intent.putExtra("productType", PRODUCT_BY_WEIGHT);
                startActivityForResult(intent, 1);
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.simple_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        finish();

        return super.onOptionsItemSelected(item);
    }

}
