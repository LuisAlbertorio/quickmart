package com.pixnabi.quickmart.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class FavoritesItemsAdapter extends BaseAdapter {
    Context mContext;
    int gridViewPosition;
    ArrayList<ParseObject> listParseObject;
    TextView productName, productDescription;
    ImageView imageView;
    GridView gridView2;
    FavoritesItemsActivity favoritesItemsActivity;

    // Constructor
    public FavoritesItemsAdapter(Context c, ArrayList<ParseObject> parseObjects, GridView gridView) {
        mContext = c;

        listParseObject = parseObjects;
        this.gridViewPosition = parseObjects.size();
        gridView2 = gridView;
    }

    public int getCount() {
        return listParseObject.size();
    }

    public Object getItem(int position) {


        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ImageButton delete;

        if (convertView == null) {

            convertView = View.inflate(mContext, R.layout.grid_item, null);

            imageView = (ImageView) convertView.findViewById(R.id.imageItems);
            productName = (TextView) convertView.findViewById(R.id.itemsName);
            productDescription = (TextView) convertView.findViewById(R.id.itemDescription);


            delete = (ImageButton) convertView.findViewById(R.id.delete);
            delete.setVisibility(View.VISIBLE);

            Log.d("VAL pos", String.valueOf(position));

            delete.setTag(position);
            final View finalConvertView = convertView;
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Deleting item.." , Toast.LENGTH_LONG).show();
                    int i = (int) v.getTag();
                    Log.d("VAL i", String.valueOf(i));
                    ParseQuery parseQuery = new ParseQuery("Favorites");
                    parseQuery.whereEqualTo("PRODUCTID", listParseObject.get(i).getParseObject("PRODUCTID"));
                    parseQuery.whereEqualTo("USERID", ParseUser.getCurrentUser());
                    parseQuery.getFirstInBackground(new GetCallback() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                            if(e == null){
                                parseObject.deleteInBackground();

                                Toast.makeText(mContext, "Item deleted", Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(mContext, "Oops something is wrong.. check log", Toast.LENGTH_LONG).show();
                                Log.d("FAVparseEXCEP", e.toString());
                            }
                        }
                    });

                    favoritesItemsActivity = (FavoritesItemsActivity) mContext;

                    favoritesItemsActivity.deletingItems(i);

//                    listParseObject.remove(i);
//                    listParseObject.remove(listParseObject.get(i));
//                    notifyDataSetChanged();
//                    gridView2.invalidateViews();
                    getView(position, finalConvertView,parent);
                    Log.d("new log", "viewsInvalidated");


                }
            });

            ParseQuery parseQuery = new ParseQuery("Products");

            try {
                Picasso.with(mContext).load(parseQuery.get(listParseObject.get(position).getParseObject("PRODUCTID").getObjectId()).getParseFile("image").getUrl()).into(imageView);
                productName.setText(parseQuery.get(listParseObject.get(position).getParseObject("PRODUCTID").getObjectId()).getString("name"));
                productDescription.setText(parseQuery.get(listParseObject.get(position).getParseObject("PRODUCTID").getObjectId()).getString("description"));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            convertView.setTag(position);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ShopItems.class);
                    int x = (int) v.getTag();
                    ParseQuery query = new ParseQuery("Products");

                    try {
                        intent.putExtra("productName", query.get(listParseObject.get(x).getParseObject("PRODUCTID").getObjectId()).getString("name"));
                        intent.putExtra("productDescription", query.get(listParseObject.get(x).getParseObject("PRODUCTID").getObjectId()).getString("description"));
                        intent.putExtra("productImage", query.get(listParseObject.get(x).getParseObject("PRODUCTID").getObjectId()).getParseFile("image").getUrl());
                        intent.putExtra("productId", listParseObject.get(x).getParseObject("PRODUCTID").getObjectId());
                        intent.putExtra("productType", 1);
                    } catch (ParseException e) {
                        e.printStackTrace();

                    }
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            });


        } else {
            //imageView = (ImageView) convertView;
        }

        return convertView;
    }

}
