package com.pixnabi.quickmart.myapplication;


import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtils {


    public static String saveToInternalSorage (Bitmap bitmapImage, String fileName, Context context){

        ContextWrapper cw = new ContextWrapper(context);
        if(cw == null)
            return null;

        try {

            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("movieposter", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, fileName);
            //Toast.makeText(context, "" + mypath, Toast.LENGTH_LONG).show();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(mypath);
                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return directory.getAbsolutePath();
        }
        catch (NullPointerException ex) {
            ex.printStackTrace();

        }
        return null;
    }


    public static Bitmap readBitmapFromInternalStorage (Context context, String fileName){

        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("movieposter", Context.MODE_PRIVATE);
        // Create imageDir
        //File mypath = new File(directory,"profile.jpg");
        File f = new File(directory, fileName);
        Bitmap b = null;
        //Toast.makeText(getBaseContext(),"" + f, Toast.LENGTH_SHORT).show();
        try {
            b = BitmapFactory.decodeStream(new FileInputStream(f));

        } catch (FileNotFoundException e1) {

            //Toast.makeText(context, "Esto se chavó", Toast.LENGTH_SHORT).show();
            e1.printStackTrace();
        }
        return b;
    }



    public static void save (String data, String file, Context context ){

        //data = et.getText().toString();
        try {
            FileOutputStream fOut = context.openFileOutput(file, Context.MODE_PRIVATE);
            fOut.write(data.getBytes());
            fOut.close();
            //Toast.makeText(context,"file saved", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String read (String file, Context context){

        String temp="";
        try{
            FileInputStream fin = context.openFileInput(file);
            int c;

            while( (c = fin.read()) != -1){
                temp = temp + Character.toString((char)c);
            }
            //et.setText(temp);
            //Toast.makeText(context,"file read", Toast.LENGTH_SHORT).show();

        }catch(Exception e){
            temp = "";

        }

        return temp;
    }



    public static void saveSetting(Context context, String newSetting) {

        save(newSetting, "appsetting", context);
    }

    public static JSONObject getSettings(Context context) {

        String settingString = read("appsetting", context);

        try {
            JSONObject settings = new JSONObject(settingString);
            return settings;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static void deleteFiles(Context context, String path) {

        ContextWrapper cw = new ContextWrapper(context);
        if(cw == null)
            return;

        //File file = new File(path);
        File directory = cw.getDir("movieposter", Context.MODE_PRIVATE);
        path = directory.getAbsolutePath();

        //Log.i("Delete Directory", path);

        if (directory.exists()) {
            String deleteCmd = "rm -r " + path;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
