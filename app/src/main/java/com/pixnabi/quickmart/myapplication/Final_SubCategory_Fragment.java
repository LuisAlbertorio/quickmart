package com.pixnabi.quickmart.myapplication;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class Final_SubCategory_Fragment extends android.support.v4.app.Fragment {

    ArrayList<String> FSIname;
    ArrayList<String> FSIdescription;
    ArrayList<String> FSIimage;
    ArrayList<String> FSIobjId;
    public static HomeActivity homeActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_sub_category, container, false);

        FSIname = getArguments().getStringArrayList("name");
        FSIdescription = getArguments().getStringArrayList("desc");
        FSIimage = getArguments().getStringArrayList("image");
        FSIobjId = getArguments().getStringArrayList("objId");

        Log.d("FinalCatNameSize",String.valueOf(FSIname.size()));

        GridView gridView = (GridView) view.findViewById(R.id.gridView);
        gridView.setBackgroundColor(Color.parseColor("#81c784"));
        gridView.setAdapter(new Final_SubCategory_GridViewAdapter(getActivity(), FSIname, FSIdescription, FSIimage, FSIobjId));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Activity activity = getActivity();

                Intent intent = new Intent(homeActivity, ShopItems.class);
                intent.putExtra("productName", FSIname.get(position));
                intent.putExtra("productDescription", FSIdescription.get(position));
                intent.putExtra("productImage", FSIimage.get(position));
                intent.putExtra("productId", FSIobjId.get(position));
                intent.putExtra("productType", 1);
                intent.putExtra("fromFavs", false);
                activity.startActivityForResult(intent, 1);
            }
        });

        homeActivity = (HomeActivity) getActivity();

        return view;
    }

    public AdapterView.OnItemClickListener onItemClickListener(){
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Activity activity = getActivity();

                Intent intent = new Intent(homeActivity, ShopItems.class);
                intent.putExtra("productName", FSIname.get(position));
                intent.putExtra("productDescription", FSIdescription.get(position));
                intent.putExtra("productImage", FSIimage.get(position));
                intent.putExtra("productId", FSIobjId.get(position));
                intent.putExtra("productType", 1);
                intent.putExtra("fromFavs", false);
                activity.startActivityForResult(intent, 1);
            }
        };
    }
}
