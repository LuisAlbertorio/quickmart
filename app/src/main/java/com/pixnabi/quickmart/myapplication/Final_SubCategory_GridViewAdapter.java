package com.pixnabi.quickmart.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class Final_SubCategory_GridViewAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater layoutInflater;
    int gridViewPosition;

    ArrayList<String> FSIname;
    ArrayList<String> FSIdescription;
    ArrayList<String> FSIimage;
    ArrayList<String> FSIobjId;

    // Constructor
    public Final_SubCategory_GridViewAdapter(Context c, ArrayList<String> FSIname,ArrayList<String> FSIdescription,ArrayList<String> FSIimage,ArrayList<String> FSIobjId) {
        mContext = c;
        this.FSIname = FSIname;
        this.FSIdescription = FSIdescription;
        this.FSIimage= FSIimage;
        this.FSIobjId= FSIobjId;

        this.gridViewPosition = gridViewPosition;
    }

    public int getCount() {
        return FSIname.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        RelativeLayout relativeLayout;
        ImageButton delete;
        TextView itemName, itemDesc;

        if (convertView == null) {

            convertView = View.inflate(mContext, R.layout.grid_item, null);

            imageView = (ImageView)convertView.findViewById(R.id.imageItems);
            itemName = (TextView)convertView.findViewById(R.id.itemsName);
            itemDesc = (TextView)convertView.findViewById(R.id.itemDescription);

            //if (gridViewPosition == 0){
//            imageView.setImageResource(mThumbIds[position]);
            //}

            Picasso.with(mContext).load(FSIimage.get(position)).into(imageView);
            itemName.setText(FSIname.get(position));
            itemDesc.setText(FSIdescription.get(position));


            convertView.setTag(gridViewPosition);


        } else {
            //imageView = (ImageView) convertView;
        }

        //imageView.setImageResource(mThumbIds[position]);
        return convertView;
    }

    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.apple, R.drawable.cocacola,
            R.drawable.carne, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne
    };
}
