package com.pixnabi.quickmart.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class FirstActivity extends Activity{

    TextView iniciarSesion, iniciarSesion2;
    String[] productNames, productDescription, productId, productImage;
    ArrayList<String> cat = new ArrayList<String>();
    int amountinCart;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);

        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");
        ParseFacebookUtils.initialize("960454483983536");

        iniciarSesion = (TextView) findViewById(R.id.textView10);
        iniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickLogIn();
            }
        });

        iniciarSesion2 = (TextView) findViewById(R.id.textView11);
        iniciarSesion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickLogIn();
            }
        });

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.pixnabi.quickmart.myapplication",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }

    public void facebookLogIn(View view){

        //final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarFirst);
        //progressBar.setVisibility(View.VISIBLE);

        //LoginActivity.this.progressDialog = ProgressDialog.show(
        //LoginActivity.this, "", "Logging in...", true);
        List<String> permissions = Arrays.asList("public_profile", "user_friends");


        ParseFacebookUtils.logIn(permissions, this, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {

                if (parseUser == null) {
                    //progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getBaseContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");

                } else if (parseUser.isNew()) {

                    //progressBar.setVisibility(View.INVISIBLE);
                    Intent i = new Intent(getBaseContext(), EnterZipcode.class);
                    startActivity(i);

                    // Fetch Facebook user info if the session is active
                    Session session = ParseFacebookUtils.getSession();
                    if (session != null && session.isOpened()) {
                        makeMeRequest();
                        // newMyFriendsRequest();
                    }

                    //overridePendingTransition(R.anim.signup_tutorial_in, R.anim.signup_tutorail_out);
                    Toast.makeText(getBaseContext(), "New User!", Toast.LENGTH_SHORT).show();
                    Log.d("MyApp", "User signed up and logged in through Facebook!");

                } else {
                    if(parseUser != null) {

                        final ProgressDialog progressDialog = new ProgressDialog(FirstActivity.this);
                        progressDialog.setMessage("logging in");
                        progressDialog.show();

                        HashMap hashMap = new HashMap<String, Object>();
                        hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());

                        ParseCloud.callFunctionInBackground("seeCategories", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
                            @Override
                            public void done(ArrayList<ParseObject> parseObjects, ParseException e) {
                                productNames = new String[parseObjects.size()];
                                productDescription = new String[parseObjects.size()];
                                productId = new String[parseObjects.size()];
                                productImage = new String[parseObjects.size()];

                                for (int i = 0; i < parseObjects.size(); i++) {
                                    productNames[i] = parseObjects.get(i).getString("name");
                                    productDescription[i] = parseObjects.get(i).getString("description");
                                    productId[i] = parseObjects.get(i).getObjectId();
                                    productImage[i] = parseObjects.get(i).getParseFile("image").getUrl();

                                    Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("name"));
                                    Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("category"));
                                    Log.d("SEE CAT DESCRIPTION", parseObjects.get(i).getString("description"));

                                    if (cat.contains(parseObjects.get(i).getString("category"))) {
                                    } else {
                                        cat.add(parseObjects.get(i).getString("category"));
                                    }
                                }
                            }
                        });

                        ParseQuery query;
                        query = ParseQuery.getQuery("Cart");
                        query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                        query.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> list, ParseException e) {
                                amountinCart = list.size();
                            }
                        });

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent i = new Intent(getBaseContext(), HomeActivity.class);
                                i.putExtra("cartAmount", amountinCart);
                                i.putStringArrayListExtra("categories", cat);
                                i.putExtra("productNames", productNames);
                                i.putExtra("productDescription", productDescription);
                                i.putExtra("productId", productId);
                                i.putExtra("productImage", productImage);
                                startActivity(i);
                                progressDialog.dismiss();
                                finish();
                            }
                        }, 3500);

                    }

                    //progressBar.setVisibility(View.INVISIBLE);
//                    Intent i = new Intent(getBaseContext(), HomeActivity.class);
//                    startActivity(i);
                    //overridePendingTransition(R.anim.signup_tutorial_in, R.anim.signup_tutorail_out);
                    Toast.makeText(getBaseContext(), "Success!!!  " + parseUser.get("authData"), Toast.LENGTH_SHORT).show();
                    Log.d("MyApp", "User logged in through Facebook!");

                }
            }
        });
    }

    private void makeMeRequest() {
        Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        if (user != null) {
                            // Create a JSON object to hold the profile info
                            JSONObject userProfile = new JSONObject();
                            try {
                                // Populate the JSON object

                                ParseUser parseUser = ParseUser.getCurrentUser();
                                //parseUser.setUsername(user.getUsername());
                                //parseUser.setPassword(passwordSignIn);
                                //parseUser.setEmail(emailSignIn);
                                parseUser.put("nameUser", user.getFirstName());
                                parseUser.put("apellidosUser", user.getLastName());
                                parseUser.saveInBackground();

                                userProfile.put("facebookId", user.getId());
                                userProfile.put("name", user.getName());

                                Toast.makeText(getBaseContext(), "" + user.getUsername(), Toast.LENGTH_SHORT).show();


                            } catch (JSONException e) {
                                //Log.d(IntegratingFacebookTutorialApplication.TAG,
                                //"Error parsing returned user data.");
                            }

                        } else if (response.getError() != null) {
                            // handle error
                        }
                    }
                });
        request.executeAsync();

    }

    public void onClickLogIn (){

        Intent intent = new Intent(this, LogInActivity.class);
        startActivity(intent);
    }

    public void onClickRegister (View view){

        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
    }
}
