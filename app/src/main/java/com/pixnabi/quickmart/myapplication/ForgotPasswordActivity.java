package com.pixnabi.quickmart.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;


public class ForgotPasswordActivity extends Activity {

    EditText emailForgot;
    String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);

        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");

        emailForgot = (EditText) findViewById(R.id.emailForgot);
    }

    public void forgotPassword (View view){

        email = emailForgot.getText().toString();

        ParseUser.requestPasswordResetInBackground(email, new RequestPasswordResetCallback() {
            @Override
            public void done(com.parse.ParseException e) {

                if (e == null) {
                    // An email was successfully sent with reset instructions.
                    Intent i = new Intent(ForgotPasswordActivity.this, LogInActivity.class);
                    startActivity(i);

                    Toast.makeText(getBaseContext(), "Check your email for reset password." ,Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getBaseContext(), "Invalid email." ,Toast.LENGTH_SHORT).show();
                    Log.v("Error: ", e.getMessage());
                    // Something went wrong. Look at the ParseException to see what's up.
                }

            }
        });
    }

}
