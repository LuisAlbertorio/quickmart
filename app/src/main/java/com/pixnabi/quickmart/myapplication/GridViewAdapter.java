package com.pixnabi.quickmart.myapplication;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GridViewAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater layoutInflater;
    int gridViewPosition;
    String cate;
    String[] nameParse;
    String[] descriptionParse;
    String[] objectid;
//    ParseFile[] parseFile;
    String[] imageParse;
    Bitmap bitmap;
    int count=0;


    // Constructor
    public GridViewAdapter(Context c, int gridViewPosition, String[] names, String[] description, String[] file, String[] object) {
        mContext = c;
        this.gridViewPosition = gridViewPosition;
        nameParse = names;
        descriptionParse = description;
        imageParse = file;
        objectid = object;

    }
    public GridViewAdapter(Context c, int gridViewPosition) {
        mContext = c;
        this.gridViewPosition = gridViewPosition;
    }

    public int getCount() {
        return 3;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView imageView;
        TextView productName, productDescription;
        RelativeLayout relativeLayout;


        if(count == 3 ) count = 0;


        if (convertView == null) {


            convertView = View.inflate(mContext, R.layout.grid_item, null);

            imageView = (ImageView) convertView.findViewById(R.id.imageItems);
            productName = (TextView) convertView.findViewById(R.id.itemsName);
            productDescription = (TextView) convertView.findViewById(R.id.itemDescription);



            Picasso.with(mContext).load(imageParse[gridViewPosition*3 + position]).into(imageView);
//            Log.d("count", String.valueOf(count));

            productName.setText(nameParse[gridViewPosition*3 + position]);
            productDescription.setText(descriptionParse[gridViewPosition*3  + position]);

            convertView.setTag(gridViewPosition);

            count++;
        }

        return convertView;
    }


}
