package com.pixnabi.quickmart.myapplication;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Handler;


public class HomeActivity extends BaseActivity {

    int items;
    String[]  subProductNames, subProductDescription, subProductId, subProductImage;
    ArrayList<String> subCat ;
    ArrayList<String> FSIname;
    ArrayList<String> FSIdescription;
    ArrayList<String> FSIimage;
    ArrayList<String> FSIobjId;
    int SUBCATEGORY_FRAGMENT = 1;
    int ITEMS_SUBCATEGORY =2;
    int fragment;
    android.support.v7.app.ActionBarDrawerToggle dToggle;
    FrameLayout frameLayout;
    Toolbar toolbar;
    MenuItem brandsMenu;
    TextView tv;
    CategoryFragmentHome fragmentHome =  new CategoryFragmentHome();
    SubCategoryFragment subCategoryFragment = new SubCategoryFragment();
    Final_SubCategory_Fragment finalsubCategoryfragment = new Final_SubCategory_Fragment();
    public int amountinCart=0;

    ArrayList<String> cats;

    TextView categories;

    boolean subcat, fsubcat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_drawer);
        //setContentView(R.layout.activity_home)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        //parse initializing
        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4" +
                "vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");

        Bundle extras = getIntent().getExtras();
        amountinCart = extras.getInt("cartAmount");



        Bundle bundle = new Bundle();
        bundle.putStringArrayList("categories", extras.getStringArrayList("categories"));
        bundle.putStringArray("productNames", extras.getStringArray("productNames"));
        bundle.putStringArray("productDescription", extras.getStringArray("productDescription"));
        bundle.putStringArray("productId", extras.getStringArray("productId"));
        bundle.putStringArray("productImage", extras.getStringArray("productImage"));

        cats = extras.getStringArrayList("categories");
        fragmentHome.setArguments(bundle);

        HashMap hashMap = new HashMap<String, Object>();
        hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_drawer);
        //toolbar.setTitle("Categorías");
        toolbar.setTitleTextColor(Color.parseColor("#616161"));
        toolbar.canShowOverflowMenu();

        frameLayout = (FrameLayout) findViewById(R.id.container);
        loadFragment(fragmentHome, false);

        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dToggle = new android.support.v7.app.ActionBarDrawerToggle(
                this,  mDrawerLayout, toolbar,
                R.string.drawer_open, R.string.drawer_close
        );
        mDrawerLayout.setDrawerListener(dToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        dToggle.syncState();

    }


    public void loadFragment (android.support.v4.app.Fragment fragment , boolean backPressed){

            // Create the transaction
        FragmentTransaction fts = getSupportFragmentManager().beginTransaction();

        // Configure the "in" and "out" animation files
        if (backPressed == false) {
            fts.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        }else {
            fts.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        }


        // Perform the fragment replacement
        fts.replace(R.id.container, fragment, "fragment");

        // Start the animated transition.
        fts.commit();

    }

    @Override
    public void onResume(){
        super.onResume();

        ParseQuery query;
        query = ParseQuery.getQuery("Cart");
        query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(amountinCart != list.size()) amountinCart = list.size();
                updateCartBadge();
            }
        });
    }


    public void verMas(int pos, boolean subcategory, boolean finalsubcategory) throws ParseException {

       if (finalsubcategory){

           subcat = subcategory;
           fsubcat = finalsubcategory;

           ParseQuery query = new ParseQuery("Products");
           query.whereEqualTo("subCategory", subCat.get(pos));
           Log.d("subcat", subCat.get(pos));
           query.findInBackground(new FindCallback<ParseObject>() {
               @Override
               public void done(List<ParseObject> list, ParseException e) {
                   if(e!=null)Log.d("PEFcat", e.toString());
                   else Log.d("List Size",String.valueOf(list.size()));

                   FSIimage = new ArrayList<>(list.size());
                   FSIname = new ArrayList<>(list.size());
                   FSIdescription = new ArrayList<>(list.size());
                   FSIobjId = new ArrayList<>(list.size());

                   for(int i =0; i < list.size(); i++){
                       Log.d("name", list.get(i).getString("name"));
                       FSIname.add(i, list.get(i).getString("name"));
                       FSIdescription.add(i, list.get(i).getString("description"));
                       FSIimage.add(i, list.get(i).getParseFile("image").getUrl());
                       FSIobjId.add(i, list.get(i).getObjectId());
                   }

                   Bundle fsubBundle = new Bundle();
                   fsubBundle.putStringArrayList("name", FSIname);
                   fsubBundle.putStringArrayList("desc", FSIdescription);
                   fsubBundle.putStringArrayList("image", FSIimage);
                   fsubBundle.putStringArrayList("objId", FSIobjId);
                   finalsubCategoryfragment.setArguments(fsubBundle);
               }
           });


           new android.os.Handler().postDelayed(new Runnable() {
               @Override
               public void run() {
                   Toast.makeText(getApplicationContext(), "Final subcat fragment", Toast.LENGTH_LONG).show();

                   loadFragment(finalsubCategoryfragment, false);
                   brandsMenu.setVisible(true);

                   fragment = ITEMS_SUBCATEGORY;
               }
           }, 500);


       }else if (subcategory){
           subcat = subcategory;
           fsubcat = finalsubcategory;
           final String catId;
           categories = (TextView)findViewById(R.id.category);
           Log.d("POS", String.valueOf(pos));
           Log.d("VER MAS CAT", cats.get(pos));
           ParseQuery query = new ParseQuery("Category");
           query.whereEqualTo("category",cats.get(pos));
           catId = query.getFirst().getObjectId();

           Log.d("catId", catId);

           ParseQuery parseQuery = new ParseQuery("SubCategory");
           parseQuery.whereEqualTo("categoryId", catId);
           parseQuery.findInBackground(new FindCallback<ParseObject>() {
               @Override
               public void done(List<ParseObject> list, ParseException e) {
                   if(e!=null)Log.d("parseVMExp", e.toString());
                   Log.d("sizeList", String.valueOf(list.size()));
                   subCat = new ArrayList<>(list.size());
                   for(int i = 0; i<list.size();i++){
                       subCat.add(list.get(i).getString("subCategory"));
                   }

                   Bundle subBundle = new Bundle();
                   subBundle.putStringArrayList("categories", subCat);
                   subCategoryFragment.setArguments(subBundle);
                   loadFragment(subCategoryFragment, false);
                   brandsMenu.setVisible(true);
                   //toolbar.setTitle("Subcategorías");
                   fragment = SUBCATEGORY_FRAGMENT;

               }
           });

       }


        Toast.makeText(getBaseContext(), "Eureka!!!", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onBackPressed() {

        if (subcat){

            //frameLayout.removeAllViews();
            subcat=false;
            loadFragment(fragmentHome, true);
            //toolbar.setTitle("Categorías");
            brandsMenu.setVisible(false);
            fragment = 0;

        }else  if (fsubcat){

            Log.d("inFSubCat", String.valueOf(subcat));
            subcat = true;
            //frameLayout.removeAllViews();
            Bundle subBundle = new Bundle();
            subBundle.putStringArrayList("categories", subCat);
            subCategoryFragment.setArguments(subBundle);
            loadFragment(subCategoryFragment, true);
            fsubcat=false;

            //toolbar.setTitle("Subcategorías");
            brandsMenu.setVisible(true);
            fragment = SUBCATEGORY_FRAGMENT;
        }

        else {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);

        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_home;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        RelativeLayout badgeLayout = (RelativeLayout) menu.findItem(R.id.cart).getActionView();

        brandsMenu = (MenuItem) menu.findItem(R.id.home);
        brandsMenu.setVisible(false);

        ImageButton cart = (ImageButton) badgeLayout.findViewById(R.id.myButton);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, CartActivity.class);
                intent.putExtra("amountincart", amountinCart);
                startActivityForResult(intent, 2);
            }
        });

        //number of items in cart
        tv = (TextView) badgeLayout.findViewById(R.id.textOne);
        if(amountinCart != 0){
            tv.setVisibility(View.VISIBLE);
            tv.setText(String.valueOf(amountinCart));
        }else{
            tv.setVisibility(View.INVISIBLE);
        }
//        tv.setText("12");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //brandsMenu = (ActionMenuItemView) findViewById(R.id.home);
        //brandsMenu.setVisibility(View.INVISIBLE);

        if (id == R.id.home) {

            loadFragment(fragmentHome, false);
            brandsMenu.setVisible(false);
            fragment = 0;

            return true;

        }else if (id == R.id.cart) {

            return true;

        }else if (id == R.id.search_bar){

            Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
            startActivity(intent);

            return true;
        }else if (id == R.id.recibo){

            Intent intent = new Intent(HomeActivity.this, OrderListActivity.class);
            startActivity(intent);

            return true;
        }else if (id == R.id.favorites){

            Intent intent = new Intent(HomeActivity.this, FavoritesItemsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        dToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        dToggle.onConfigurationChanged(newConfig);
    }

    public void updateCartBadge(){
        tv.setText(String.valueOf(amountinCart));
        if(tv.getVisibility() == View.INVISIBLE){
            tv.setVisibility(View.VISIBLE);
        }

        if(amountinCart == 0){
            tv.setVisibility(View.INVISIBLE);
        }
    }

    public void onActivityResult(int requestCode, int resultcode, Intent data){
        if(requestCode == 1){
            if(resultcode == RESULT_OK){
                amountinCart = amountinCart + 1;
                updateCartBadge();
            }
        }

        if(requestCode == 2){
            if(resultcode == RESULT_OK){
                amountinCart = data.getExtras().getInt("amountincart");
                updateCartBadge();
            }
        }
    }
}