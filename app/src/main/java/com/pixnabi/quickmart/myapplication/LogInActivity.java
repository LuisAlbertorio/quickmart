package com.pixnabi.quickmart.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LogInActivity extends Activity {

    EditText email, password;
    String emailLogIn, passwordLogIn;
    TextView forgotPassword, logInTitle;
    Button logInButton;
    String[] productNames, productDescription, productId, productImage;
    ArrayList<String> cat = new ArrayList<String>();
    int amountinCart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in_activity);

        //getActionBar().hide();

        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");
        ParseFacebookUtils.initialize("960454483983536");

        //Typeface gM = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");
        //Typeface gB = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Book.otf");

        ParseUser currentUser = ParseUser.getCurrentUser();
        if ((currentUser != null)){

            Intent i = new Intent(getBaseContext(), HomeActivity.class);
            startActivity(i);
            //overridePendingTransition(R.anim.signup_tutorial_in, R.anim.signup_tutorail_out);
            // Go to the user info activity
            //Toast.makeText(getBaseContext(), "Jump Success!!!", Toast.LENGTH_SHORT).show();

        }

        email = (EditText) findViewById(R.id.emailLogIn);
        //email.setTypeface(gM);

        password = (EditText) findViewById(R.id.passwordLogIn);
        //password.setTypeface(gM);

        forgotPassword = (TextView) findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogInActivity.this, ForgotPasswordActivity.class);
                startActivity(i);
            }
        });
        //forgotPassword.setTypeface(gB);

        //logInTitle = (TextView) findViewById(R.id.logInTitle);
        //logInTitle.setTypeface(gB);

        logInButton = (Button) findViewById(R.id.log_in_button);
        //logInButton.setTypeface(gM);


    }

    public void logIn(View view){

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBarLogIn);
        progressBar.setVisibility(View.VISIBLE);

        emailLogIn = email.getText().toString();
        passwordLogIn = password.getText().toString();

        ParseUser.logInInBackground(emailLogIn, passwordLogIn, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, com.parse.ParseException e) {

                if (parseUser != null) {

//                    final ProgressDialog progressDialog = new ProgressDialog(LogInActivity.this);
//                    progressDialog.setMessage("logging in");
//                    progressDialog.show();

//                    progressBar.setVisibility(View.INVISIBLE);

                    HashMap hashMap = new HashMap<String, Object>();
                    hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());

                    ParseCloud.callFunctionInBackground("seeCategories", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
                        @Override
                        public void done(ArrayList<ParseObject> parseObjects, ParseException e) {
                            productNames = new String[parseObjects.size()];
                            productDescription = new String[parseObjects.size()];
                            productId = new String[parseObjects.size()];
                            productImage = new String[parseObjects.size()];

                            for (int i = 0; i < parseObjects.size(); i++) {
                                productNames[i] = parseObjects.get(i).getString("name");
                                productDescription[i] = parseObjects.get(i).getString("description");
                                productId[i] = parseObjects.get(i).getObjectId();
                                productImage[i] = parseObjects.get(i).getParseFile("image").getUrl();

                                Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("name"));
                                Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("category"));
                                Log.d("SEE CAT DESCRIPTION", parseObjects.get(i).getString("description"));

                                if (cat.contains(parseObjects.get(i).getString("category"))) {
                                } else {
                                    cat.add(parseObjects.get(i).getString("category"));
                                }
                            }
                        }
                    });

                    ParseQuery query;
                    query = ParseQuery.getQuery("Cart");
                    query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                    query.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> list, ParseException e) {
                            amountinCart = list.size();
                        }
                    });

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(getBaseContext(), HomeActivity.class);
                            i.putExtra("cartAmount", amountinCart);
                            i.putStringArrayListExtra("categories", cat);
                            i.putExtra("productNames", productNames);
                            i.putExtra("productDescription", productDescription);
                            i.putExtra("productId", productId);
                            i.putExtra("productImage", productImage);
                            startActivity(i);
                            finish();
//                            progressDialog.dismiss();
                        }
                    },3500);
                    // Hooray! The user is logged in.

                } else {

                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getBaseContext(), "No se pudo iniciar sesión", Toast.LENGTH_SHORT).show();
                    Log.v("Error: ", e.getMessage());
                    // Signup failed. Look at the ParseException to see what happened.
                }
            }
        });

    }

    public void facebookClick (View view){

        List<String> permissions = Arrays.asList("public_profile", "user_friends");


        ParseFacebookUtils.logIn(permissions, this, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {

                if (parseUser == null) {
                    //progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getBaseContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");

                } else if (parseUser.isNew()) {

                    //progressBar.setVisibility(View.INVISIBLE);
                    Intent i = new Intent(getBaseContext(), EnterZipcode.class);
                    startActivity(i);
                    //overridePendingTransition(R.anim.signup_tutorial_in, R.anim.signup_tutorail_out);
                    Toast.makeText(getBaseContext(), "Nuevo Usuario!", Toast.LENGTH_SHORT).show();
                    Log.d("MyApp", "User signed up and logged in through Facebook!");

                } else {

//                    final ProgressDialog progressDialog = new ProgressDialog(LogInActivity.this);
//                    progressDialog.setMessage("logging in");
//                    progressDialog.show();

                    HashMap hashMap = new HashMap<String, Object>();
                    hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());

                    ParseCloud.callFunctionInBackground("seeCategories", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
                        @Override
                        public void done(ArrayList<ParseObject> parseObjects, ParseException e) {
                            productNames = new String[parseObjects.size()];
                            productDescription = new String[parseObjects.size()];
                            productId = new String[parseObjects.size()];
                            productImage = new String[parseObjects.size()];

                            for (int i = 0; i < parseObjects.size(); i++) {
                                productNames[i] = parseObjects.get(i).getString("name");
                                productDescription[i] = parseObjects.get(i).getString("description");
                                productId[i] = parseObjects.get(i).getObjectId();
                                productImage[i] = parseObjects.get(i).getParseFile("image").getUrl();

                                Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("name"));
                                Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("category"));
                                Log.d("SEE CAT DESCRIPTION", parseObjects.get(i).getString("description"));

                                if (cat.contains(parseObjects.get(i).getString("category"))) {
                                } else {
                                    cat.add(parseObjects.get(i).getString("category"));
                                }
                            }
                        }
                    });

                    ParseQuery query;
                    query = ParseQuery.getQuery("Cart");
                    query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                    query.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> list, ParseException e) {
                            amountinCart = list.size();
                        }
                    });

                    //progressBar.setVisibility(View.INVISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(getBaseContext(), HomeActivity.class);
                            i.putExtra("cartAmount", amountinCart);
                            i.putStringArrayListExtra("categories", cat);
                            i.putExtra("productNames", productNames);
                            i.putExtra("productDescription", productDescription);
                            i.putExtra("productId", productId);
                            i.putExtra("productImage", productImage);
                            startActivity(i);
//                            progressDialog.dismiss();
                            finish();
                        }
                    },3500);
                    //overridePendingTransition(R.anim.signup_tutorial_in, R.anim.signup_tutorail_out);
                    Toast.makeText(getBaseContext(), "Exito!", Toast.LENGTH_SHORT).show();
                    Log.d("MyApp", "User logged in through Facebook!");

                }
            }
        });
    }

    public void register (View view){

        Intent i = new Intent(LogInActivity.this, SignUpActivity.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();

        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
        //overridePendingTransition (R.anim.slide_up_back, R.anim.slide_down_back);
    }
}