package com.pixnabi.quickmart.myapplication;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class NavigationFragment extends Fragment {

    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    ParseUser currentUser;
    TextView nombreUsuario;
    String username, nombre, lastname;
    TextView dirrecUser, creditcardUser, logout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.test_list_drawer, container, false);

        Parse.initialize(getActivity(), "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");
        currentUser = ParseUser.getCurrentUser();

        if(currentUser.get("nameUser") == null) nombre = (String) currentUser.get("name");
        else nombre = (String) currentUser.get("nameUser");


        if(currentUser.get("apellidosUser") == null) lastname = (String) currentUser.get("lastName");
        else lastname = (String) currentUser.get("apellidosUser");


        username = nombre + " " + lastname;

        nombreUsuario = (TextView) view.findViewById(R.id.nombreUsuario);
        dirrecUser = (TextView)view.findViewById(R.id.direccionUser);
        logout = (TextView)view.findViewById(R.id.cerrarSesion);
        creditcardUser = (TextView)view.findViewById(R.id.tarjetasUser);
        nombreUsuario.setText(username);

        dirrecUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddressActivity.class);
                startActivity(intent);
            }
        });

        creditcardUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreditCardActivity.class);
                startActivity(intent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser.logOut();
                Intent intent = new Intent(getActivity(), FirstActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });


        return view;
    }

    //this is called when I do an action in the other fragment
    public void setScreen(int id) {
        //This fragment is one of the 5 possible fragments
        //DetailFragment2 newF = new DetailFragment2();
        //FragmentTransaction transaction = getFragmentManager().beginTransaction();
        //transaction.replace(R.id.listB, newF);
        //transaction.addToBackStack(null);
        //transaction.commit();
    }

}
