package com.pixnabi.quickmart.myapplication;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class NoConnection extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);
    }

}
