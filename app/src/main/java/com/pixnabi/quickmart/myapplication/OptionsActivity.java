package com.pixnabi.quickmart.myapplication;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseUser;

public class OptionsActivity extends Activity {

    Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.options_activity);

        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");

    }

    public void backArrow (View view){

        finish();
    }

    public void accountOnClick (View view){

        Intent i = new Intent(this, AccountActivity.class);
        startActivity(i);
    }

    public void addressOnClick (View view){

        Intent i = new Intent(this, AddressListActivity.class);
        startActivity(i);
    }

    public void logout (View view){

        ParseUser.logOut();
        //finish();
        Intent i = new Intent(this, LogInActivity.class);
        startActivity(i);
        Toast.makeText(getBaseContext(), "Cerrando Sesión", Toast.LENGTH_SHORT).show();
    }

    public void creditCardOnClick(View view) {

        Intent i = new Intent(this, CreditCardListActivity.class);
        startActivity(i);
    }
}
