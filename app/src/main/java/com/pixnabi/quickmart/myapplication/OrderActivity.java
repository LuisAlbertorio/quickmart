package com.pixnabi.quickmart.myapplication;


import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderActivity extends BaseActivity {

    ParseUser currentUser;
    String orderId;
    String total;

    TextView orderTotal;
    RecyclerView mRecyclerView;
    OrderRecycleViewAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    ArrayList<ParseObject> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);
        //setContentView(R.layout.activity_home)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        //parse initializing
        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");
        currentUser = ParseUser.getCurrentUser();

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            orderId = extras.getString("orderId");
            total = extras.getString("orderTotal");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Orden #" + orderId);
        toolbar.setTitleTextColor(Color.parseColor("#616161"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        orderTotal = (TextView) findViewById(R.id.total);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);


        orderTotal.setText(total);

        // specify an adapter (see also next example)
        mAdapter = new OrderRecycleViewAdapter(OrderActivity.this);
        mRecyclerView.setAdapter(mAdapter);

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("userId", currentUser.getObjectId());
        hashMap.put("orderId", orderId);

        ParseCloud.callFunctionInBackground("getUserItemsWithOrderId", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {

            public void done(ArrayList<ParseObject> result, ParseException e) {
                if (e == null) {
                    items = result;
                    Log.d("productsOrder",String.valueOf(result.size()));
                    mAdapter.isShoppingCart = false;
                    mAdapter.reloadWithData(items);
                } else {
                    // an error occur
                    Toast.makeText(getApplicationContext(), "Error Message: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onClickRowCart (View view){
        //here we do nothing
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.order_activity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_direction, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        finish();

        return super.onOptionsItemSelected(item);
    }
}
