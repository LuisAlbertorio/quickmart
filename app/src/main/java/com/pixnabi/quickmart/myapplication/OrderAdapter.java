package com.pixnabi.quickmart.myapplication;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private String[] mDataset;
    private int[] mCars;
    public ArrayList<ParseObject> shoppingCart;
    Context mContext;

    // Provide a suitable constructor (depends on the kind of dataset)
    public OrderAdapter(Context context) {
        //mDataset=myDataset;
        //mCars = cars;
        mContext = context;
    }

    public void reloadWithData(ArrayList<ParseObject> shoppingCart) {
        this.shoppingCart = shoppingCart;

        notifyDataSetChanged();
    }

// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        RelativeLayout relativeLayout;
        TextView itemQuantity;
        TextView itemName;
        TextView itemDescription;
        TextView itemPrice;
        ImageView itemImage;

        public ViewHolder(View v) {
            super(v);

            relativeLayout = (RelativeLayout) v.findViewById(R.id.relativeRowCart);
            itemQuantity = (TextView) v.findViewById(R.id.item_quantity);
            itemName = (TextView) v.findViewById(R.id.item_name);
            itemDescription = (TextView) v.findViewById(R.id.item_description);
            //itemPrice = (TextView) v.findViewById(R.id.item_price);
            itemImage = (ImageView) v.findViewById(R.id.item_imageview);

        }

    }

    // Create new views (invoked by the layout manager)
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_items_row, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        ParseObject item = shoppingCart.get(position + 1);
        holder.relativeLayout.setTag(position);
        if(item.getString("productType").contentEquals("by unit")) {
            holder.itemQuantity.setText("" + item.getInt("quantity"));
        }
        else {
            holder.itemQuantity.setText("" + item.getDouble("quantity"));
        }

        holder.itemName.setText(item.getString("name"));
        holder.itemDescription.setText(item.getString("description"));

        ParseFile photoFile = item.getParseFile("image");
        Picasso.with(mContext).load(photoFile.getUrl()).into(holder.itemImage);
    }

    @Override
    public int getItemCount() {
        if(shoppingCart == null || shoppingCart.size() == 0) {
            return 0;
        }

        return shoppingCart.size() - 1;
    }
}
