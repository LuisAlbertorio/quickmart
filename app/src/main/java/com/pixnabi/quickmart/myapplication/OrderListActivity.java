package com.pixnabi.quickmart.myapplication;


import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderListActivity extends BaseActivity {

    Toolbar toolbar;
    RecyclerView mRecyclerView;
    OrderListAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    TextView noOrderText;

    ParseUser currentUser;
    ArrayList<ParseObject> orders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);
        //setContentView(R.layout.account_activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        //parse initializing
        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");

        //get curent user
        currentUser = ParseUser.getCurrentUser();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Ordenes");
        toolbar.setTitleTextColor(Color.parseColor("#616161"));
        toolbar.canShowOverflowMenu();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycleView);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new OrderListAdapter(this);
        mRecyclerView.setAdapter(mAdapter);


        //get user orders from parse
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("userId", currentUser.getObjectId());

        ParseQuery query = new ParseQuery("Order");
        query.whereEqualTo("userId", currentUser.getObjectId());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                orders = (ArrayList<ParseObject>) list;
                Log.d("orderListSize",String.valueOf(list.size()));
                mAdapter.reloadWithData(orders);
            }
        });




    }

    @Override
    protected int getLayoutResource() {
        return R.layout.order_list_activity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.simple_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        finish();

        return super.onOptionsItemSelected(item);
    }
}
