package com.pixnabi.quickmart.myapplication;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;

import java.util.ArrayList;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder> implements View.OnClickListener {

    //product measure type constants
    final int PRODUCT_BY_QUANTITY = 1;
    final int PRODUCT_BY_WEIGHT = 2;

    //private String[] mDataset;
    //private int[] mCars;
    public ArrayList<ParseObject> orders;
    private Activity activity;
    Context mContext;
    View v;

    // Provide a suitable constructor (depends on the kind of dataset)
    public OrderListAdapter(Context context, String[] myDataset) {
       // mDataset = myDataset;
        //mCars = cars;
        mContext = context;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public OrderListAdapter(Context context) {
        mContext = context;
    }

    public void reloadWithData(ArrayList<ParseObject> orders) {
        this.orders = orders;
        notifyDataSetChanged();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        //ImageView imageView;
        TextView orderId;
        TextView orderTotal;
        TextView orderDate;
        TextView orderStatus;
        //GridView gridview;
        //Button viewMore;
        //ImageView inCartImage;



        public ViewHolder(View v) {
            super(v);

            orderId = (TextView) v.findViewById(R.id.numeroOrden);
            orderTotal = (TextView) v.findViewById(R.id.order_total);
            orderDate = (TextView) v.findViewById(R.id.order_date);
            orderStatus = (TextView) v.findViewById(R.id.orderStatus);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OrderListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_id_row, parent, false);
        // set the view's size, margins, paddings and layout parameters

        v.setOnClickListener(OrderListAdapter.this);

        ViewHolder vh = new ViewHolder(v);

        vh.orderId.setOnClickListener(OrderListAdapter.this);
        vh.orderId.setTag(vh);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        //set tag with position to get correctly data from array when onclicklistener
        v.setTag(position);

        // - get element from your array data at this position
        // - replace the contents of the view with that element

        //get order from array
        ParseObject order = orders.get(position);

        //set row with order data
        holder.orderId.setText(order.getObjectId());
        holder.orderTotal.setText("$" + String.format("%.2f", order.getDouble("total")));
        holder.orderStatus.setText(order.getString("status"));
        holder.orderDate.setVisibility(View.INVISIBLE);

        //falta mostrar fecha

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if(orders == null || orders.size() == 0) {
            return 0;
        }

        Log.d("itemsCount", String.valueOf(orders.size()));

        return orders.size();
    }

    @Override
    public void onClick(View view) {

        Toast.makeText(mContext, "Orden #" + view.getTag(), Toast.LENGTH_SHORT).show();

        //get order of the row clicked
        ParseObject order = orders.get(Integer.parseInt(view.getTag().toString()));

        Intent intent = new Intent(mContext, OrderActivity.class);
        intent.putExtra("orderId", order.getObjectId());
        intent.putExtra("orderTotal", "$" + String.format("%.2f", order.getDouble("total")) );
        mContext.startActivity(intent);

        /*if (view.getId() == holder.numeroOrden.getId()) {
            Toast.makeText(mContext, "Click!", Toast.LENGTH_SHORT).show();
        }*/
    }
}
