package com.pixnabi.quickmart.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Luis on 4/27/2016.
 */
public class OrderRecycleViewAdapter extends RecyclerView.Adapter<OrderRecycleViewAdapter.ViewHolder> {
    //private String[] mDataset;
    //private int[] mCars;
    public ArrayList<ParseObject> shoppingCart;
    public boolean isShoppingCart;
    Context mContext;
    TextView total;
    public ParseObject itemForDel;
    public CartActivity cartActivity;

    public OrderRecycleViewAdapter(Context context){
        //mDataset=myDataset;
        //mCars = cars;

        //is true order is pending so we can delete article is user shopping cart otherwise is a buyed order and items cannot be deleted
        isShoppingCart = true;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_items_row, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        RelativeLayout relativeLayout;
        TextView itemQuantity;
        TextView itemName;
        TextView itemDescription;
        TextView itemPrice;
        ImageView itemImage;
        ImageButton itemDelete;


        public ViewHolder(View v) {
            super(v);

            relativeLayout = (RelativeLayout) v.findViewById(R.id.relativeRowCart);
            itemQuantity = (TextView) v.findViewById(R.id.item_quantity);
            itemName = (TextView) v.findViewById(R.id.item_name);
            itemDescription = (TextView) v.findViewById(R.id.item_description);
            //itemPrice = (TextView) v.findViewById(R.id.item_price);
            itemImage = (ImageView) v.findViewById(R.id.item_imageview);
            itemDelete = (ImageButton) v.findViewById(R.id.item_delete);
            total = (TextView)v.findViewById(R.id.total);
        }

    }

    public void reloadWithData(ArrayList<ParseObject> shoppingCart2) {
        Log.d("reloadSize", String.valueOf(shoppingCart2.size()));
        shoppingCart = shoppingCart2;

        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        ParseObject item = shoppingCart.get(position);
        holder.relativeLayout.setTag(position);
        if(item.getString("productType").contentEquals("by unit")) {
            holder.itemQuantity.setText("" + item.getInt("quantity"));
        }
        else {
            holder.itemQuantity.setText("" + item.getDouble("quantity"));
        }

        if(isShoppingCart == false) {
            //hide delete button
            holder.itemDelete.setVisibility(View.INVISIBLE);
        }

        holder.itemName.setText(item.getString("name"));
        holder.itemDescription.setText(item.getString("description"));

        ParseFile photoFile = item.getParseFile("image");
        Picasso.with(mContext).load(photoFile.getUrl()).into(holder.itemImage);

    }

    @Override
    public int getItemCount() {
        if(shoppingCart == null || shoppingCart.size() == 0) {
            return 0;
        }

        return shoppingCart.size() ;
    }
}


