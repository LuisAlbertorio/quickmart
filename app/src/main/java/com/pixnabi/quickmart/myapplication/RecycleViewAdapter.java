package com.pixnabi.quickmart.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;

import java.util.ArrayList;


public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder> {

    //product measure type constants
    final int PRODUCT_BY_QUANTITY = 1;
    final int PRODUCT_BY_WEIGHT = 2;
    String itemId;
    String[] names;
    String[] descriptions;
    String[] image;
    String[] objectId;
    static Button viewMore;
    public static HomeActivity homeActivity;

    private String[] mDataset;
    static ArrayList<String> cate;
    private int[] mCars;
    Context mContext;

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecycleViewAdapter(Context context, ArrayList<String> cat, String[] productNames, String[] productDescription, String[] file, String[] productId) {
        cate = cat;
//        Log.d("RECYCLE VIEW ADAPTER", cate.toString());
        mContext = context;
        names = productNames;
        descriptions = productDescription;
        image = file;
        objectId = productId;
        homeActivity = (HomeActivity) context;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        ImageView imageView;
        TextView textView;
        GridView gridview;
        ImageView inCartImage;




        public ViewHolder(View v) {
            super(v);

            textView = (TextView) v.findViewById(R.id.category);
            //imageView = (ImageView) v.findViewById(R.id.imageView);
            gridview = (GridView) v.findViewById(R.id.gridViewItems);
            viewMore = (Button) v.findViewById(R.id.buttonViewMore);
            viewMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        homeActivity.verMas(getPosition(), true, false);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
            //cardCategory = (CardView) v.findViewById(R.id.cardCategory);

//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    HomeActivity homeActivity = new HomeActivity();
//                    homeActivity.verMas(v,cate.get(getPosition()));
//                }
//            });

        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycleview_row, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        holder.textView.setText(cate.get(position).toString());
       // Toast.makeText(mContext, cate.get(position).toString(), Toast.LENGTH_LONG).show();
        //holder.imageView.setImageResource(mCars[position]);
        holder.gridview.setAdapter(new GridViewAdapter(mContext, position, names, descriptions, image, objectId));
        holder.gridview.setOnItemClickListener(onItemGridViewClick());

        if (position == 0) {

            holder.gridview.setBackgroundColor(Color.parseColor("#81c784"));
            holder.textView.setBackgroundColor(Color.parseColor("#81c784"));
            //holder.viewMore.setTextColor(Color.parseColor("#81c784"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });

        } else if (position == 1) {

            //holder.gridview.setBackgroundColor(Color.argb(255, 144, 164, 174));
            //holder.textView.setBackgroundColor(Color.argb(255, 144, 164, 174));
            holder.gridview.setBackgroundColor(Color.parseColor("#e57373"));
            holder.textView.setBackgroundColor(Color.parseColor("#e57373"));
            //holder.viewMore.setTextColor(Color.argb(255, 144, 164, 174));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });

        }else if (position == 2) {

            holder.gridview.setBackgroundColor(Color.parseColor("#fbc02d"));
            holder.textView.setBackgroundColor(Color.parseColor("#fbc02d"));
            //holder.viewMore.setTextColor(Color.parseColor("#e57373"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });
        }
        else if (position == 3) {

            holder.gridview.setBackgroundColor(Color.parseColor("#BA68C8"));
            holder.textView.setBackgroundColor(Color.parseColor("#BA68C8"));
            //holder.viewMore.setTextColor(Color.parseColor("#64b5f6"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });

        }else if (position == 4) {

            holder.gridview.setBackgroundColor(Color.parseColor("#900D47A1"));
            holder.textView.setBackgroundColor(Color.parseColor("#900D47A1"));
            //holder.viewMore.setTextColor(Color.parseColor("#fff176"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });

        }else if (position == 5) {

            holder.gridview.setBackgroundColor(Color.parseColor("#9e9e9e"));
            holder.textView.setBackgroundColor(Color.parseColor("#9e9e9e"));
            //holder.viewMore.setTextColor(Color.parseColor("#4dd0e1"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });

        }else if (position == 6) {

            holder.gridview.setBackgroundColor(Color.parseColor("#FF8A65"));
            holder.textView.setBackgroundColor(Color.parseColor("#FF8A65"));
            //holder.viewMore.setTextColor(Color.parseColor("#a1887f"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });

        }else if (position == 7) {

            holder.gridview.setBackgroundColor(Color.parseColor("#8d6e63"));
            holder.textView.setBackgroundColor(Color.parseColor("#8d6e63"));
            //holder.viewMore.setTextColor(Color.parseColor("#f06292"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });

        }else if (position == 8) {

            holder.gridview.setBackgroundColor(Color.parseColor("#26c6da"));
            holder.textView.setBackgroundColor(Color.parseColor("#26c6da"));
            //holder.viewMore.setTextColor(Color.parseColor("#ffb74d"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });

        }else if (position == 9) {

            holder.gridview.setBackgroundColor(Color.parseColor("#FF8F00"));
            holder.textView.setBackgroundColor(Color.parseColor("#FF8F00"));
            //holder.viewMore.setTextColor(Color.parseColor("#9e9e9e"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });
        }else if (position == 10) {

            holder.gridview.setBackgroundColor(Color.parseColor("#ec407a"));
            holder.textView.setBackgroundColor(Color.parseColor("#ec407a"));
            //holder.viewMore.setTextColor(Color.parseColor("#9e9e9e"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });
        }else if (position == 11) {

            holder.gridview.setBackgroundColor(Color.parseColor("#78909C"));
            holder.textView.setBackgroundColor(Color.parseColor("#78909C"));
            //holder.viewMore.setTextColor(Color.parseColor("#9e9e9e"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });
        }else if (position == 12) {

            holder.gridview.setBackgroundColor(Color.parseColor("#D4E157"));
            holder.textView.setBackgroundColor(Color.parseColor("#D4E157"));
            //holder.viewMore.setTextColor(Color.parseColor("#9e9e9e"));
//            viewMore.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homeActivity = (HomeActivity) mContext;
//                    homeActivity.verMas(v, cate.get(position));
//                }
//            });
        }

    }

    public AdapterView.OnItemClickListener onItemGridViewClick(){

        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Activity activity = (Activity) mContext;

                //final AlertDialog alertHorarioEntrega = new AlertDialog.Builder(activity).create();
                //alertHorarioEntrega.show();
                //alertHorarioEntrega.setContentView(R.layout.item_shop);

                Intent intent = new Intent(homeActivity, ShopItems.class);
                intent.putExtra("productName", names[position + 3*Integer.parseInt(view.getTag().toString())]);
                intent.putExtra("productDescription", descriptions[position + 3*Integer.parseInt(view.getTag().toString())]);
                intent.putExtra("productImage", image[position + 3*Integer.parseInt(view.getTag().toString())]);

                intent.putExtra("productId", objectId[position + 3*Integer.parseInt(view.getTag().toString())]);
                intent.putExtra("productType", PRODUCT_BY_QUANTITY);
                intent.putExtra("fromFavs", false);
                activity.startActivityForResult(intent, 1);

             //   Toast.makeText(mContext, "Grid Item Position: " + position + " --- " + view.getTag(), Toast.LENGTH_SHORT).show();
            }
        };
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return cate.size();
    }
}
