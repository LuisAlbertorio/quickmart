package com.pixnabi.quickmart.myapplication;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;


public class SearchActivity extends BaseActivity {

    final int SHOPITEM_ACTIVITY_REQUEST = 1;

    //product measure type constants
    final int PRODUCT_BY_QUANTITY = 1;
    final int PRODUCT_BY_WEIGHT = 2;

    GridView gridView;
    SearchAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);
        //setContentView(R.layout.item_shop);

        //parse initializing
        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_grey);
        toolbar.setTitle("Busqueda");
        toolbar.setTitleTextColor(Color.parseColor("#616161"));
        toolbar.canShowOverflowMenu();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        gridView = (GridView) findViewById(R.id.gridView);
        adapter = new SearchAdapter(this, this);
        //gridView.setBackgroundColor(Color.parseColor("#81c784"));
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(adapter.onItemClickListener());

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.search_activity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_items, menu);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

            SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

            SearchView search = (SearchView) menu.findItem(R.id.search_bar).getActionView();

            search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

            search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String s) {

                    //search item Query
                    HashMap<String, Object> hashMap = new HashMap<String, Object>();
                    hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());
                    hashMap.put("name", s);
                    //hashMap.put("category", "Frutas");
                    //hashMap.put("subCategory", "Vegetales Secos");

                    ParseCloud.callFunctionInBackground("searchProduct", hashMap, new FunctionCallback<ArrayList>() {

                        public void done(ArrayList result, ParseException e) {
                            if (e == null) {

                                //reload gridview with de items that match the search
                                adapter.reloadWithItems(result);
                                // gridView.invalidateViews();

                                if(result == null || result.size() == 0) {
                                    Toast.makeText(getApplicationContext(), "No result", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                // an error occur
                               Toast.makeText(getApplicationContext(), "Error Message: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {

                    //load results
                    //Toast.makeText(getBaseContext(), "Search: " + query, Toast.LENGTH_SHORT).show();
                    return true;
                }

            });

        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == SHOPITEM_ACTIVITY_REQUEST) {

            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                int position = data.getExtras().getInt("itemPosition", -1);
                int productType = data.getExtras().getInt("productType", -1);
                boolean inCart = data.getExtras().getBoolean("inCart", false);

                if(position >= 0 && productType != -1) {
                    //valid position, we update item
                    //gridView.getItemAtPosition(position);
                    //gridView.getFirstVisiblePosition();
                    //gridView.getChildAt(position);
                    //gridView.getChildCount();

                    SearchAdapter adapter = (SearchAdapter) gridView.getAdapter();
                    ParseObject item = adapter.items.get(position % adapter.items.size());
                    item.put("inCart", inCart);

                    if(productType == PRODUCT_BY_QUANTITY) {
                        item.put("quantity", data.getExtras().getDouble("quantity"));
                        Log.i("Item new quantity", "" + data.getExtras().getDouble("quantity"));
                    }
                    else {
                        item.put("quantity", data.getExtras().getDouble("weight"));
                        Log.i("Item new weight", "" + data.getExtras().getDouble("weight"));

                    }

                    //update grid cell
                    int firstVisibleChildPosition = gridView.getFirstVisiblePosition();
                    View v = gridView.getChildAt(position - firstVisibleChildPosition);
//                    ImageView check = (ImageView) v.findViewById(R.id.in_cart);

                    if(inCart == true) {
//                        check.setVisibility(View.VISIBLE);
                    }
                    else {
//                        check.setVisibility(View.INVISIBLE);
                    }

                    Toast.makeText(this, "pos: " + position + " itemPos: " + position % adapter.items.size()  + " visible: " + gridView.getFirstVisiblePosition() + " count: " + gridView.getChildCount(), Toast.LENGTH_LONG).show();
                }

            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

       // Toast.makeText(this, "Search: ", Toast.LENGTH_SHORT).show();
        if (id == R.id.search_bar) {

            return true;
        }else {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
