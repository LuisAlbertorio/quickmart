package com.pixnabi.quickmart.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class SearchAdapter extends BaseAdapter {

    final int SHOPITEM_ACTIVITY_REQUEST = 1;

    Activity activity;
    Context mContext;
    LayoutInflater layoutInflater;
    int gridViewPosition;
    ArrayList<ParseObject> items;

    // Constructor
    public SearchAdapter(Context c ) {
        mContext = c;
        items = new ArrayList<ParseObject>();
        this.gridViewPosition = gridViewPosition;
    }

    public SearchAdapter(Context c, Activity activity) {
        this.activity = activity;
        mContext = c;
        items = new ArrayList<ParseObject>();
        this.gridViewPosition = gridViewPosition;
    }

    public int getCount() {
        if(items == null)
            return 0;

        return items.size();
    }

    public void reloadWithItems(ArrayList<ParseObject> items) {

        this.items = items;
        notifyDataSetInvalidated();
        //notifyDataSetChanged();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderItem viewHolder;


        if (convertView == null) {

            convertView = View.inflate(mContext, R.layout.grid_item, null);

            // well set up the ViewHolder
            viewHolder = new ViewHolderItem();
            viewHolder.name = (TextView) convertView.findViewById(R.id.itemsName);
            viewHolder.description = (TextView) convertView.findViewById(R.id.itemDescription);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.imageItems);
//            viewHolder.inCartImage = (ImageView) convertView.findViewById(R.id.in_cart);

            // store the holder with the view.
            convertView.setTag(viewHolder);

            /*convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ShopItems.class);
                    //intent.putExtra("productType", PRODUCT_BY_WEIGHT);
                    mContext.startActivity(intent);
                }
            });*/

        } else {
            // we've just avoided calling findViewById() on resource everytime
            // just use the viewHolder
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        //get the corresponding item data by position
        ParseObject item = items.get(position);

        //set the corresponding data in grid cell
        viewHolder.name.setText( item.getString("name") );
        viewHolder.description.setText( item.getString("description") );

        //show check icon if user have this product in cart, otherwise hide icon
        if(item.getBoolean("inCart") == true) {
           // viewHolder.inCartImage.setVisibility(View.VISIBLE);
        }
        else {
         //   viewHolder.inCartImage.setVisibility(View.INVISIBLE);
        }

        //get and set product image
        ParseFile photoFile = item.getParseFile("image");
        Picasso.with(mContext).load(photoFile.getUrl()).into(viewHolder.image);


        //Log.i("Count ", "" + item.getDouble("quantity") + " unit: " + item.getInt("quantity"));
        return convertView;
    }

    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.apple, R.drawable.cocacola,
            R.drawable.carne, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne,
            R.drawable.apple, R.drawable.carne
    };

    static class ViewHolderItem {
        TextView name;
        TextView description;
        ImageView image;
        ImageButton delete;  //not used here! This is used in favorites so user can remove it.
        ImageView inCartImage;

    }


    public AdapterView.OnItemClickListener onItemClickListener() {


        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                boolean inCart;
                double quantity;
                int productType = 1;
                ParseObject item = items.get(position);

                inCart = item.getBoolean("inCart");
                quantity = item.getDouble("quantity");
                if(item.getString("productType").contains("by weight"))
                    productType = 2;

                Toast.makeText(mContext, "Grid Item Position: " + position + " --- " + view.getTag(), Toast.LENGTH_SHORT).show();
                Toast.makeText(mContext, "itemId: " + item.getObjectId() + "q: " + quantity + " incart: " + inCart, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(mContext, ShopItems.class);
                intent.putExtra("itemId", item.getObjectId());
                intent.putExtra("itemPosition", position);
                intent.putExtra("productType", productType);
                intent.putExtra("inCart", inCart);
                intent.putExtra("quantity", quantity);
                //mContext.startActivity(intent);
                activity.startActivityForResult(intent, SHOPITEM_ACTIVITY_REQUEST);


                /*ParseObject item = items.get(position % items.size());
                List<ParseObject> itemsInCart;
                boolean inCart = false;
                double quantity = 0;
                int productType = 1;

                ParseQuery<ParseObject> query = ParseQuery.getQuery("Cart");
                query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                query.whereEqualTo("productId", item.getObjectId());

                try {
                    itemsInCart = query.find();
                } catch (ParseException e) {
                    e.printStackTrace();
                    return;
                }

                if(item.getString("productType").contains("by weight"))
                    productType = 2;

                if(itemsInCart.size() == 1) {
                    ParseObject myItem = itemsInCart.get(0);
                    inCart = true;
                    quantity = myItem.getDouble("quantity");

                    //if(productType == 1) {
                    //    intent.putExtra("quantity", myItem.getInt("quantity"));
                    //}
                    //else {
                    //    intent.putExtra("", myItem.getDouble("quantity"));
                    //}
                }


                Intent intent = new Intent(mContext, ShopItems.class);
                intent.putExtra("itemId", item.getObjectId());
                intent.putExtra("productType", productType);
                intent.putExtra("inCart", inCart);
                intent.putExtra("quantity", quantity);
                mContext.startActivity(intent);

                Toast.makeText(mContext, "Grid Item Position: " + position + " --- " + view.getTag(), Toast.LENGTH_SHORT).show();
                */

            }
        };
    }

}
