package com.pixnabi.quickmart.myapplication;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;


import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


public class ShopItems extends BaseActivity {


    //product measure type constants
    final int PRODUCT_BY_QUANTITY = 1;
    final int PRODUCT_BY_WEIGHT = 2;

    ImageButton plus, minus;
    TextView counter;

    int productType;
    int quantity;
    float weight;

    boolean inCart;
    int currentQuantity;
    float currentWeight;

    String itemId, mane, description, ptype;
    int itemPosition;

    ImageView imageView;

    TextView nameProduct, descripProduct, tv;
    String image;

    LinearLayout relatedProductList;
    int cartSize=0;

    ParseObject itemSelected;

    boolean fromFavs = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_arrow_left_grey);
        //setContentView(R.layout.item_shop);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        //parse initializing
        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            //Toast.makeText(this, "get data", Toast.LENGTH_SHORT).show();
            itemId = extras.getString("productId");
            itemPosition = extras.getInt("itemPosition", -1);
            productType = extras.getInt("productType", PRODUCT_BY_QUANTITY);
            inCart = extras.getBoolean("inCart");
            mane = extras.getString("productName");
            image = extras.getString("productImage");
            description = extras.getString("productDescription");
            if(extras.getBoolean("fromFavs")){
                fromFavs = true;
            }

           // Toast.makeText(this, "Product type: " + productType, Toast.LENGTH_SHORT).show();

            if(inCart == true) {
                if(productType == PRODUCT_BY_QUANTITY) {
                    currentQuantity = (int) extras.getDouble("quantity");
                }
                else {
                    currentWeight = (float) extras.getDouble("quantity");
                }
            }
            else {
                //not in cart we set default values
                currentWeight = 1.0f;
                currentQuantity = 1;
            }
        }
        else {
            Toast.makeText(this, "no data", Toast.LENGTH_SHORT).show();
            //this cannot happen!
            itemId = null;
            itemPosition = -1;
            productType = PRODUCT_BY_QUANTITY;
            inCart = false;
            currentQuantity = 1;
            currentWeight = 1.0f;
        }

        weight = currentWeight;
        quantity = currentQuantity;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_grey);
        toolbar.canShowOverflowMenu();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        imageView = (ImageView)findViewById(R.id.imageView);
        Picasso.with(this).load(image).into(imageView);


        nameProduct = (TextView)findViewById(R.id.nameProduct);
        descripProduct = (TextView)findViewById(R.id.descriptionProduct);

        plus = (ImageButton) findViewById(R.id.plus);
        minus = (ImageButton) findViewById(R.id.minus);
        counter = (TextView) findViewById(R.id.counter);

        nameProduct.setText(mane);
        descripProduct.setText(description);


        relatedProductList = (LinearLayout) findViewById(R.id.related_product_list);



        //set product quantity or weight
        if(productType == PRODUCT_BY_WEIGHT) {
            //weight = 1;
            counter.setText(Float.toString(weight));
        }
        else {
            counter.setText(Integer.toString(quantity));
        }



        LayoutInflater inflater = (LayoutInflater)this.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        int [] images = {R.drawable.banana,R.drawable.uvas,R.drawable.papaya,R.drawable.kiwii,R.drawable.naranjas};
        String [] name = {"Banana", "Uvas", "Papaya", "Kiwi", "Naranja"};

        for(int i = 0; i < 5; i++) {
            View view = inflater.inflate(R.layout.related_product_row, null, false);
            view.setTag(i);
            ImageView productImage = (ImageView) view.findViewById(R.id.productImage);
            TextView productName = (TextView) view.findViewById(R.id.productName);

            productImage.setBackgroundResource(images[i]);
            productName.setText(name[i]);

            relatedProductList.addView(view);
        }

    }

    public void getproduct(){

    }

    public void addToCart(View view) {
        Toast.makeText(getApplicationContext(), "Add to product", Toast.LENGTH_SHORT).show();
        if(itemId == null || itemId.length() == 0) {
            return;
        }


        HashMap hashMap = new HashMap<String, Object>();
        hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());
        hashMap.put("productId", itemId);
        if(productType == PRODUCT_BY_QUANTITY) {
            hashMap.put("quantity", quantity);

            if(quantity != 0) {
                inCart = true;
            }
            else {
                inCart = false;
            }
        }
        else {
            hashMap.put("quantity", weight);

            if(weight != 0.0) {
                inCart = true;
            }
            else {
                inCart = false;
            }
        }

       // Toast.makeText(getApplicationContext(), "user id: " + ParseUser.getCurrentUser().toString() + "poduct id: " + itemId + " quantity: " + quantity + " weight: " + weight, Toast.LENGTH_SHORT).show();

        ParseCloud.callFunctionInBackground("addProductToCart", hashMap, new FunctionCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {

                if (e == null) {

//                    Toast.makeText(getApplicationContext(), "Product: " + parseObject.toString(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();

                    intent.putExtra("itemPosition", itemPosition);
                    intent.putExtra("productType", productType);
                    intent.putExtra("inCart", inCart);

                    if(productType == PRODUCT_BY_QUANTITY) {
                        intent.putExtra("quantity", quantity);
                    }
                    else {
                        intent.putExtra("weight", (double) weight);
                    }

                    setResult(RESULT_OK, intent);
                    Toast.makeText(getApplicationContext(), "Añadido al carrito.", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    // an error occur
                    Toast.makeText(getApplicationContext(), "Error Message: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void plus (View view){


        if(productType == PRODUCT_BY_QUANTITY) {
            quantity++;
            counter.setText(String.valueOf(quantity));
        }
        else {
            weight += 0.25;
            counter.setText(Float.toString(weight));
        }


    }

    public void minus (View view){


        if(productType == PRODUCT_BY_QUANTITY) {
            if (quantity <= 0) {

                counter.setText("0");

            } else
                quantity--;
            counter.setText(String.valueOf(quantity));
        }
        else {
            if(weight <= 0) {
                counter.setText("0");
            }
            else {
                weight -= 0.25;
                if(weight <= 0)
                    counter.setText("0");
                else
                    counter.setText(Float.toString(weight));

            }
        }

    }

    public void relatedProductSelected(View view) {
//        Toast.makeText(this, "Yes, product id " + view.getTag(), Toast.LENGTH_SHORT).show();

        if (quantity <= 0){

            counter.setText("0");

        }else
        quantity--;
        counter.setText(String.valueOf(quantity));


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.item_shop;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shop_items, menu);

        RelativeLayout badgeLayout = (RelativeLayout) menu.findItem(R.id.amountCart).getActionView();

        tv = (TextView) badgeLayout.findViewById(R.id.textOne);

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());

        ParseCloud.callFunctionInBackground("shoppingCart", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {

            public void done(ArrayList<ParseObject> result, ParseException e) {
                if (e == null) {

                    if (result.size() > 0) {
                        ParseObject order = result.get(0);
                        tv.setText("$" + String.format("%.2f", order.getDouble("total")));

                    } else {
                        tv.setText("$0.00");
                    }

                } else {
                    // an error occur
                    Toast.makeText(getApplicationContext(), "Error Message: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        //tv.setText("$112.00");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Intent myIntent = new Intent(this, HomeActivity.class);
        //startActivityForResult(myIntent, 0);

        if (id == R.id.amountCart) {
            return true;
        }else if(id == R.id.favorites_shop_items){

//            ParseQuery parseQuery = new ParseQuery("Favorites");
//            parseQuery.whereEqualTo("USERID", ParseUser.getCurrentUser());
//            parseQuery.whereEqualTo("PRODUCTID", itemId);
//            if(parseQuery == null){
//                Toast.makeText(this, "Ya esta en tus favoritos", Toast.LENGTH_LONG).show();
//            }else{
//
//            }

            ParseQuery query = new ParseQuery("Products");
            query.whereEqualTo("objectId", itemId);
            query.getFirstInBackground(new GetCallback() {
                @Override
                public void done(final ParseObject parseObject1, ParseException e) {
                    ParseQuery parseQuery = new ParseQuery("Favorites");
                    parseQuery.whereEqualTo("USERID", ParseUser.getCurrentUser());
                    parseQuery.whereEqualTo("PRODUCTID", parseObject1);
                    parseQuery.getFirstInBackground(new GetCallback() {
                        @Override
                        public void done(ParseObject parseObject2, ParseException e) {
                            if(parseObject2 == null){
                                itemSelected = parseObject1;
                                ParseObject fav = new ParseObject("Favorites");
                                fav.put("PRODUCTID", itemSelected);
                                fav.put("USERID", ParseUser.getCurrentUser());
                                fav.saveInBackground(new SaveCallback(){
                                    @Override
                                    public void done(ParseException e) {
                                        Toast.makeText(getApplicationContext(), "Added to favorites", Toast.LENGTH_LONG).show();
                                        if(e!=null) Log.d("Parse Exception", e.toString());
                                    }
                                });
                            }else{
                                Toast.makeText(getApplicationContext(), "Already in Favorites", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            });
        }else{
            finish();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
