package com.pixnabi.quickmart.myapplication;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SignUpActivity extends Activity {

    ImageView profilePicImageView , userProfilePic;
    EditText name, email, password, apellidos, phone, zipcode;
    TextView signUp;
    String nameSignIn, emailSignIn, passwordSignIn, apellidosSignIn, phoneSignIn, zipcodeSignIN;
    String[] productNames, productDescription, productId, productImage;
    ArrayList<String> cat = new ArrayList<String>();
    int amountinCart;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_activity);

        //getActionBar().hide();

        //Typeface gM = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Medium.otf");
        //Typeface gB = Typeface.createFromAsset(getAssets(), "fonts/Gotham-Book.otf");

        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");

        //signUp = (TextView) findViewById(R.id.sign);
        //signUp.setTypeface(gB);

        name = (EditText) findViewById(R.id.nameSignIn);
        //name.setTypeface(gM);

        email = (EditText) findViewById(R.id.emailSignIn);
        //email.setTypeface(gM);

        password = (EditText) findViewById(R.id.passwordSignIn);
        //password.setTypeface(gM);

        apellidos = (EditText) findViewById(R.id.apellidosSignIn);

        zipcode = (EditText) findViewById(R.id.zipcodeSignIn);

        phone = (EditText) findViewById(R.id.telefonoSignIn);

    }

    public void onClickSignUp(View view){

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        nameSignIn = name.getText().toString();
        emailSignIn = email.getText().toString();
        passwordSignIn = password.getText().toString();
        apellidosSignIn = apellidos.getText().toString();
        phoneSignIn = phone.getText().toString();
        zipcodeSignIN = zipcode.getText().toString();
        //Bitmap bitmap = ((BitmapDrawable) userProfilePic.getDrawable()).getBitmap();

        //ByteArrayOutputStream stream = new ByteArrayOutputStream();
        //bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        //byte[] bytesArray = stream.toByteArray();


        ParseUser user = new ParseUser();
        user.setUsername(emailSignIn);
        user.setPassword(passwordSignIn);
        user.setEmail(emailSignIn);
        user.put("nameUser", nameSignIn);
        user.put("apellidosUser", apellidosSignIn);
        user.put("zipcodeUser", zipcodeSignIN);
        user.put("telefonoUser", phoneSignIn);

// other fields can be set just like with ParseObject
        //user.put("phone", "650-253-0000");

        user.signUpInBackground(new SignUpCallback() {
              @Override
              public void done(com.parse.ParseException e) {

                  if (e == null) {
                      if(ParseUser.getCurrentUser() != null) {
                          progressBar.setVisibility(View.INVISIBLE);

                          final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this);
                          progressDialog.setMessage("logging in");
                          progressDialog.show();

                          HashMap hashMap = new HashMap<String, Object>();
                          hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());

                          ParseCloud.callFunctionInBackground("seeCategories", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
                              @Override
                              public void done(ArrayList<ParseObject> parseObjects, ParseException e) {
                                  productNames = new String[parseObjects.size()];
                                  productDescription = new String[parseObjects.size()];
                                  productId = new String[parseObjects.size()];
                                  productImage = new String[parseObjects.size()];

                                  for (int i = 0; i < parseObjects.size(); i++) {
                                      productNames[i] = parseObjects.get(i).getString("name");
                                      productDescription[i] = parseObjects.get(i).getString("description");
                                      productId[i] = parseObjects.get(i).getObjectId();
                                      productImage[i] = parseObjects.get(i).getParseFile("image").getUrl();

                                      Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("name"));
                                      Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("category"));
                                      Log.d("SEE CAT DESCRIPTION", parseObjects.get(i).getString("description"));

                                      if (cat.contains(parseObjects.get(i).getString("category"))) {
                                      } else {
                                          cat.add(parseObjects.get(i).getString("category"));
                                      }
                                  }
                              }
                          });

                          ParseQuery query;
                          query = ParseQuery.getQuery("Cart");
                          query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                          query.findInBackground(new FindCallback<ParseObject>() {
                              @Override
                              public void done(List<ParseObject> list, ParseException e) {
                                  amountinCart = list.size();
                              }
                          });

                          new Handler().postDelayed(new Runnable() {
                              @Override
                              public void run() {
                                  Intent i = new Intent(getBaseContext(), HomeActivity.class);
                                  i.putExtra("cartAmount", amountinCart);
                                  i.putStringArrayListExtra("categories", cat);
                                  i.putExtra("productNames", productNames);
                                  i.putExtra("productDescription", productDescription);
                                  i.putExtra("productId", productId);
                                  i.putExtra("productImage", productImage);
                                  startActivity(i);
                                  progressDialog.dismiss();
                                  finish();
                              }
                          }, 3500);


                      }
//                      Intent i = new Intent(getBaseContext(), HomeActivity.class);
//                      startActivity(i);
                      //overridePendingTransition(R.anim.signup_tutorial_in, R.anim.signup_tutorail_out);

                      // Hooray! Let them use the app now.
                  } else {
                      progressBar.setVisibility(View.INVISIBLE);
                      Toast.makeText(getBaseContext(), "Unable to sign in.", Toast.LENGTH_SHORT).show();
                      Log.v("Error: ", e.getMessage());
                      // Sign up didn't succeed. Look at the ParseException
                      // to figure out what went wrong
                  }

              }
          });

    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        //overridePendingTransition (R.anim.slide_up_back, R.anim.slide_down_back);
    }
}

