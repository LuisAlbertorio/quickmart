package com.pixnabi.quickmart.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Splash extends ActionBarActivity {

    String[] productNames, productDescription, productId, productImage;
    int amountinCart;
    ArrayList<String> cat = new ArrayList<String>();
    boolean moo = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Parse.initialize(this, "wuCefJ99wqoH940a9KkNTJ7Xwz7obhw5j4" +
                "vPNN1z", "HA8Gel1roYgaPl23zRbAevPKWUBnIb1IfSBLAGmX");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(CheckNetwork.isInternetAvailable(Splash.this)) //returns true if internet available
                {

                    if(ParseUser.getCurrentUser() != null) {
                        HashMap hashMap = new HashMap<String, Object>();
                        hashMap.put("userId", ParseUser.getCurrentUser().getObjectId());

                        ParseCloud.callFunctionInBackground("seeCategories", hashMap, new FunctionCallback<ArrayList<ParseObject>>() {
                            @Override
                            public void done(ArrayList<ParseObject> parseObjects, ParseException e) {
                                if(e!=null) Log.d("ParseException", e.toString());
                                moo = false;
                                productNames = new String[parseObjects.size()];
                                productDescription = new String[parseObjects.size()];
                                productId = new String[parseObjects.size()];
                                productImage = new String[parseObjects.size()];

                                for (int i = 0; i < parseObjects.size(); i++) {
                                    productNames[i] = parseObjects.get(i).getString("name");
                                    productDescription[i] = parseObjects.get(i).getString("description");
                                    productId[i] = parseObjects.get(i).getObjectId();
                                    productImage[i] = parseObjects.get(i).getParseFile("image").getUrl();

                                    Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("name"));
                                    Log.d("SEE CATEGORIES FUNCTION", parseObjects.get(i).getString("category"));
                                    Log.d("SEE CAT DESCRIPTION", parseObjects.get(i).getString("description"));

                                    if (cat.contains(parseObjects.get(i).getString("category"))) {
                                    } else {
                                        cat.add(parseObjects.get(i).getString("category"));
                                    }
                                }

                                ParseQuery query;
                                query = ParseQuery.getQuery("Cart");
                                query.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                                query.findInBackground(new FindCallback<ParseObject>() {
                                    @Override
                                    public void done(List<ParseObject> list, ParseException e) {
                                        amountinCart = list.size();
                                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                        intent.putExtra("cartAmount", amountinCart);
                                        intent.putStringArrayListExtra("categories", cat);
                                        intent.putExtra("productNames", productNames);
                                        intent.putExtra("productDescription", productDescription);
                                        intent.putExtra("productId", productId);
                                        intent.putExtra("productImage", productImage);
                                        startActivity(intent);
                                    }
                                });

                            }

                        });


                    }else{
                        Intent intent = new Intent(getApplicationContext(), FirstActivity.class);
                        startActivity(intent);
                        finish();
                    }        }
                else
                {
                    Intent intent = new Intent(Splash.this, NoConnection.class);
                    startActivity(intent);
                    finish();
                }

            }
        }, 1000);




    }

}
