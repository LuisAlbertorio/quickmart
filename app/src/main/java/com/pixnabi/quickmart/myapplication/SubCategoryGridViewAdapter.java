package com.pixnabi.quickmart.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Luis on 5/6/2016.
 */
public class SubCategoryGridViewAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater layoutInflater;
    int gridViewPosition;
    static String[] nameParse;
    String[] descriptionParse;
    String[] objectid;
    String[] imageParse;
    static TextView productName;
    TextView productDescription;
    ArrayList<String> cat;

    // Constructor
    public SubCategoryGridViewAdapter(Context c, int gridViewPosition, String[] names, String[] description, String[] file, String[] object, ArrayList<String> cate) {
        mContext = c;
        this.gridViewPosition = gridViewPosition;
        nameParse = names;
        descriptionParse = description;
        imageParse = file;
        objectid = object;
        cat = cate;
    }

    public SubCategoryGridViewAdapter(Context c, int gridViewPosition) {
        mContext = c;
        this.gridViewPosition = gridViewPosition;
    }

    public int getCount() {
        return 3;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView imageView;

        RelativeLayout relativeLayout;

        if (convertView == null) {

            convertView = View.inflate(mContext, R.layout.grid_item, null);

            imageView = (ImageView) convertView.findViewById(R.id.imageItems);
            productName = (TextView) convertView.findViewById(R.id.itemsName);
            productDescription = (TextView) convertView.findViewById(R.id.itemDescription);

            Picasso.with(mContext).load(imageParse[position]).into(imageView);
            productName.setText(nameParse[position]);
            productDescription.setText(descriptionParse[position]);

            convertView.setTag(gridViewPosition);

            Log.d("nameParse", nameParse[position]);

        }

        return convertView;
    }

    public static String getInfo(int pos, int tag){
//        String info = nameParse[tag*3 + pos];

        return (String) productName.getText();
    }

}
