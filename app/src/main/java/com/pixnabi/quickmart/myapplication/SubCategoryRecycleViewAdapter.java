package com.pixnabi.quickmart.myapplication;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class SubCategoryRecycleViewAdapter extends RecyclerView.Adapter<SubCategoryRecycleViewAdapter.ViewHolder>{
    Context mContext;
    String [] pNames;
    String [] pDescription;
    String [] pId;
    String [] pImage;
    ArrayList<String> cate;
    String [] fullPnames;
    public static HomeActivity homeActivity;

    // Provide a suitable constructor (depends on the kind of dataset)
public SubCategoryRecycleViewAdapter(Context context,ArrayList<String> cat){

    cate = cat;
    fullPnames = new String[cate.size()];

    mContext=context;

    homeActivity = (HomeActivity) context;
}

// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder

public static class ViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case

    ImageView imageView;
    TextView textView;
    GridView gridview;
    Button viewMore;

    public ViewHolder(View v) {
        super(v);

        textView = (TextView) v.findViewById(R.id.category);
        gridview = (GridView) v.findViewById(R.id.gridViewItems);
        viewMore = (Button) v.findViewById(R.id.buttonViewMore);
        viewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    homeActivity.verMas(getPosition(), false, true);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

    // Create new views (invoked by the layout manager)
    @Override
    public SubCategoryRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycleview_row, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        ParseQuery query = new ParseQuery("Products");
        query.whereEqualTo("subCategory", cate.get(position));
        query.setLimit(3);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e==null){
                    //Setting the size for the arrays
//                    pId = pImage = pDescription = pNames = new String[list.size()];
                    pId = new String[list.size()];
                    pImage = new String[list.size()];
                    pDescription = new String[list.size()];
                    pNames = new String[list.size()];

                    for(int i = 0;i<list.size();i++){
                        pId[i] = list.get(i).getObjectId();
                        pNames[i] = list.get(i).getString("name");
                        pDescription[i] = list.get(i).getString("description");
                        pImage[i] = list.get(i).getParseFile("image").getUrl();
                    }

                    holder.gridview.setAdapter(new SubCategoryGridViewAdapter(mContext, position, pNames, pDescription, pImage, pId, cate));

                }else{
                    Toast.makeText(mContext, "Error: " + e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });

        holder.textView.setText(cate.get(position));
        holder.textView.setTextColor(Color.parseColor("#000000"));
        holder.textView.setBackgroundColor(Color.parseColor("#fafafa"));
        holder.gridview.setOnItemClickListener(onItemGridViewClick());

        holder.viewMore.setTextColor(Color.parseColor("#000000"));
        holder.viewMore.setBackgroundResource(R.drawable.ver_mas_negro);
    }

    // Not Working :(
    public AdapterView.OnItemClickListener onItemGridViewClick(){

        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                String info;
                info = SubCategoryGridViewAdapter.getInfo(position, Integer.parseInt(view.getTag().toString()));

                Log.d("INFO",info);

                ParseQuery query = new ParseQuery("Products");
                query.whereEqualTo("subCategory", cate.get(Integer.parseInt(view.getTag().toString())));
                query.setLimit(3);
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> list, ParseException e) {
                        Intent intent = new Intent(mContext, ShopItems.class);
                        intent.putExtra("productName", list.get(position).getString("name"));
                        intent.putExtra("productDescription", list.get(position).getString("description"));
                        intent.putExtra("productImage", list.get(position).getParseFile("image").getUrl());
                        intent.putExtra("productId", list.get(position).getObjectId());
                        intent.putExtra("productType", 1);
                        mContext.startActivity(intent);
                    }
                });

//                Toast.makeText(mContext, "Grid Item Position: " + position + " --- " + view.getTag(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return cate.size();
    }
}

